#!/bin/bash
# Basic while loop


driver_names='ElmoDriver__1 ElmoDriver__2 ElmoDriver__3 ElmoDriver__4'
shmem_postfix='_rxpdo _txpdo'

# rxpdo:
# - {pdo_subindex: 1, value_id: Controlword,      value_index: 0x160A, value_subindex: 1, value_type: UNSIGNED16}
# - {pdo_subindex: 2, value_id: Target torque,    value_index: 0x160C, value_subindex: 1, value_type: INTEGER16}
# - {pdo_subindex: 3, value_id: Target position,  value_index: 0x160F, value_subindex: 1, value_type: INTEGER32}
# - {pdo_subindex: 4, value_id: Velocity Offset,  value_index: 0x1617, value_subindex: 1, value_type: INTEGER32}
# - {pdo_subindex: 5, value_id: Torque Offset,    value_index: 0x1618, value_subindex: 1, value_type: INTEGER16}
# - {pdo_subindex: 6, value_id: Target Velocity,  value_index: 0x161C, value_subindex: 1, value_type: INTEGER32}
# - {pdo_subindex: 7, value_id: Digital Output,   value_index: 0x161D, value_subindex: 1, value_type: UNSIGNED32}
# 
# txpdo:
# - {pdo_subindex: 1, value_id: Statusword,                     value_index: 0x1A0A, value_subindex: 1, value_type: UNSIGNED16}
# - {pdo_subindex: 2, value_id: Position actual value,          value_index: 0x1A0E, value_subindex: 1, value_type: INTEGER32}
# - {pdo_subindex: 3, value_id: Velocity sensor actual value,   value_index: 0x1A0F, value_subindex: 1, value_type: INTEGER32}
# - {pdo_subindex: 4, value_id: Torque actual value,            value_index: 0x1A12, value_subindex: 1, value_type: INTEGER16}
# - {pdo_subindex: 5, value_id: Digital inputs,                 value_index: 0x1A1C, value_subindex: 1, value_type: UNSIGNED32}
  

counter=1
while [ $counter -le 1000000000 ]
do
  echo "-----------------------------------------------------$counter"
  counter=$((counter+1))
  
  for driver_name in $driver_names
  do
    echo $driver_name
    fn="/dev/shm/$driver_name""_rxpdo"
    hexdump -e '1/1 "bonded:0x%01x" 1/1 " rt: 0x%01x" 1/8 " time: 0x%04f" 1/2 " ctrl:0x%04x"  1/2 " trq:0x%04x" 1/4 " pos:0x%04x" 1/4 " vof:0x%04x" 1/2 " tof:0x%04x" 1/4 " vtrg:0x%04x" 1/4 " Dig Out:0x%04x" ' -e '"\n"' $fn
    fn="/dev/shm/$driver_name""_txpdo"
    hexdump -e '1/1 "bonded:0x%01x" 1/1 " rt: 0x%01x" 1/8 " time: 0x%04f" 1/2 " stts:0x%04x"  1/4 " pos:0x%04x" 1/4 " vel:0x%04x" 1/2 " trq:0x%04x" 1/4 " Dig In:0x%04x"' -e '"\n"' $fn
  done
  sleep 1
  
done
echo All done

#     echo "--$driver_name"
#     fn="/dev/shm/$driver_name""_rxpdo"
#     echo ">> $fn"
#     hexdump -s 6 -v -e '1/2 " ctrl:0x%02x"  1/4 " trq:0x%02x" 1/4 " pos 0x%04x" 1/4 "vel off 0x%04x" 1/2 "trq off 0x%02x" 1/4" "vel trg 0x%04x" 1/4 "Dig Out 0x%04x"' -e '"\n"' $fn
#     fn="/dev/shm/$driver_name""_txpdo"
#     echo ">> $fn"
#     

