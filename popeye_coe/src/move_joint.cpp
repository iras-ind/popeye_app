
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <signal.h>

#include <ros/ros.h>
 

#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <coe_driver/CoeMessage.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_hw_plugins/coe_ds402_plugin.h>
#include <geometry_msgs/Twist.h>
#include <realtime_tools/realtime_publisher.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "coe";

double rpm2radps( double rpm ) { return rpm * 2.0 * M_PI / 60.0 ; }
  
static const double         ACC_DEC_RAD_S2 = rpm2radps( 0.1 ) ; // Input: RPM/s, Output: rad/s^2

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

bool   active_driver_[3] = {false, false, false};

bool stop_threads = false;

ros::Subscriber coe_target_vel_;

geometry_msgs::Twist cmdvel;
ros::Time  cmd_timestamp ;

double ref_vel[3]= {0,0,0};

boost::thread_group threads;
constexpr bool smooth_f(false);

bool homing_complete[3] = {false,false,false};

constexpr double scale = 0.2;
float disp;

double dumpFactor( ) 
{
  double ret = 0.0;
  const double no_dump_time = 0.050;
  const double full_dump_time = 0.500;
  double t = (ros::Time::now() - cmd_timestamp).toSec();
  if      ( t < no_dump_time )    ret = 1.0;
  else if ( t < full_dump_time )  ret = ( t - no_dump_time ) / ( full_dump_time - no_dump_time );
  else                            ret = 0.0;
  
  return ret;
}

double smooth( const double target, const double actual_vel, const double dump_factor )
{
  double delta              = target - actual_vel; // how much is the step velocity?
  double new_target         = actual_vel + ( std::fabs( delta ) < ACC_DEC_RAD_S2 ? delta : sgn( delta ) * ACC_DEC_RAD_S2 ); // add the step filtered at max acc/dec
  double dumped_target      = dump_factor * new_target; // reduce the vel if no new references are received
  double dumped_delta       = dumped_target - actual_vel; // // how much is the dumped step velocity?maybe the dump time we super impose are too narrow .. safe check!
  double new_dumped_target  = actual_vel + ( std::fabs( dumped_delta ) < ACC_DEC_RAD_S2 ? dumped_delta : sgn( dumped_delta ) * ACC_DEC_RAD_S2 ); // add the step filtered at max acc/dec
  return new_dumped_target;
}

// void setCoeTargetVelocity(geometry_msgs::Twist cmdvel_msg)
// {
//   cmd_timestamp = ros::Time::now();
//   cmdvel = cmdvel_msg;
// }


// bool setCoeCommand ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
// {
//   ROS_INFO ( "%s<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s",BOLDCYAN(),RESET());
//   ROS_INFO ( "Received the Coe Command '%s', module address: %d", req.message.c_str(), req.addr );
//   coe_core::ds402::CommandID      cmd;
//   coe_core::ds402::status_word_t  status_word = ((coe_hw_plugins::DS402Complete*)driver_[req.addr].get())->getStatusWord();
//   coe_core::ds402::StateID        start_state  = coe_core::ds402::to_stateid ( status_word );
//   std::cout << "Actual Status: '"<< BOLDYELLOW() << coe_core::ds402::to_string(start_state) << RESET() << "'" << std::endl;
//   try
//   {
//     cmd = coe_core::ds402::to_commandid ( req.message );
//     std::cout << "Parsed comand: '" << BOLDYELLOW() << coe_core::ds402::to_string( cmd) << RESET() << std::endl;
//   }
//   catch ( std::exception& e )
//   {
//     res.message += "Exception:" + std::string ( e.what() );
//     res.message += "----\n";
//     res.message += "Req. Command: " + std::string ( req.message ) + "\n";
//     res.message +=  coe_core::ds402::echo_feasible_commands ( start_state );
//     res.success = false;
//     return true;
//   }
// 
//   try
//   {
// 
//     if(( coe_core::ds402::get_next_state ( start_state, cmd ) == start_state )
//     && ( cmd != coe_core::ds402::CMD_LOOPBACK ) && ( cmd != coe_core::ds402::CMD_SWITCH_ON ) && ( cmd != coe_core::ds402::CMD_COMPLETE_RESET ) )
//     {
//         res.message  = "Error, the command '" + req.message +"' cannot be applied\n" + coe_core::ds402::echo_feasible_commands ( start_state );
//         res.success = true;
//     }
//     else
//     {
//       coe_core::ds402::StateID next_state = coe_core::ds402::get_next_state ( start_state, cmd );
//       std::cout << "Next Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
//       
//       coe_core::ds402::control_word_t control_word;
//       coe_core::ds402::to_control_word( cmd, (uint16_t*)&control_word );
//       std::cout << coe_core::ds402::to_string(control_word,'s') << std::endl;
//       ((coe_hw_plugins::DS402Complete*)driver_[req.addr].get())->setControlWord( control_word );
// 
//       res.message  = "None";
//       res.success = true;
//       
//       ROS_WARN ( "*** Wait for the state transition.." );
//       coe_core::ds402::StateID act_state; 
//       ros::Rate r(10);
//       do 
//       {
//         r.sleep();
//         act_state = coe_core::ds402::to_stateid ( ((coe_hw_plugins::DS402Complete*)driver_[req.addr].get())->getStatusWord() );
//         if( act_state != start_state )
//         {
//           std::cout << "------------"<<std::endl;
//           std::cout << "Old Status:   '"  << BOLDYELLOW() << coe_core::ds402::to_string(start_state) << RESET() << std::endl;
//           std::cout << "Act Status:   '"  << BOLDYELLOW() << coe_core::ds402::to_string(act_state) << RESET() << std::endl;
//           std::cout << "what_is_happen: " << coe_core::ds402::what_is_happen( start_state, act_state, 1);
//           std::cout << "Next Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
//           std::cout << "------------"<<std::endl;
//         }
//         if(  act_state == next_state )
//           break;
//         
//         if( ( act_state == coe_core::ds402::STATE_FAULT ) && (cmd != coe_core::ds402::CMD_FAULT_RESET) ) 
//         {
//           res.message  += "System failure";
//           res.success = false;
//           return true;
//         }
//       }
//       while( 1 );
//       std::cout << coe_core::ds402::echo_feasible_commands ( act_state  ) <<std::endl;
//     }
// 
//     ROS_INFO ( "%s>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",BOLDCYAN(),RESET());
//     return true;
//   }
//   catch ( std::exception& e )
//   {
//     res.message  += "Req. Command: " + std::string ( req.message.data() );
//     res.message  += "Exception caught :" + std::string ( e.what() );
//     res.success = false;
//     return true;
//   }
// }

ros::Time start;
  
bool update()
{
//   cmd_timestamp = ros::Time::now();
  bool ok = true;
  double dump_factor = dumpFactor();
 
//  ref_vel[0]=  0.3 * scale * sin((ros::Time::now()-start).toSec()* 0.5);
//  ref_vel[1]=  0.3 * scale * sin((ros::Time::now()-start).toSec()* 0.5);
//  ref_vel[2]=  0.3 * scale * sin((ros::Time::now()-start).toSec()* 0.5);
  ref_vel[0]=  0.1;
              
  return ok;
}

void axMainThread()
{
  
  ros::WallRate rt(1000);
  bool ok = true;
  while(ros::ok() && !stop_threads )
  {  
    cmd_timestamp = ros::Time::now();
  
    if( !update() )
    {
      if( ok ) 
      {
        ROS_WARN("Update failed");
        ok = false;
      }
    }
    else
    {
      if( !ok )
        ROS_WARN("Update recovered");
      ok = true;
    }
    rt.sleep();
  }
  return;
}

void wheel_coe(int addr,ros::NodeHandle nh)
{
  pluginlib::ClassLoader  <coe_driver::CoeHwPlugin>       loader_("coe_driver", "coe_driver::CoeHwPlugin");
  boost::shared_ptr       <coe_driver::CoeHwPlugin>       base_driver_;
  coe_hw_plugins::DS402Complete*                          driver_;
  
  double* pos_msr_;
  double* vel_msr_;
  double* eff_msr_;
  double* pos_cmd_;
  double* vel_cmd_;
  double* eff_cmd_;

//   ros::ServiceServer    coe_command_service = nh.advertiseService ( "ax"+std::to_string(addr)+"_set_coe_command",    &setCoeCommand);
  ros::Rate RT(1000);
  
  try
  {
      
    base_driver_ = loader_.createInstance ( "coe_hw_plugins::DS402Complete" );
    driver_ = (coe_hw_plugins::DS402Complete*)base_driver_.get();
  
    if( driver_->initialize ( nh, "/eureca_master/coe/", addr+4) )
    {
      driver_->jointStateHandle  ( &pos_msr_, &vel_msr_, &eff_msr_);
      driver_->jointCommandHandle( &pos_cmd_, &vel_cmd_, &eff_cmd_);
    }
    
    if(!driver_->setTargetState( "MOO_CYCLIC_SYNCHRONOUS_VELOCITY" ))
    {
      ROS_WARN_STREAM("Set Target State failed...");
      stop_threads = true;
      return;
    }

    
    if(!driver_->setHardRT())
    {
      ROS_WARN_STREAM("Set HardRT failed...");
      stop_threads = true;
      return;
    }
    
    active_driver_[addr] = true;
    
    * pos_msr_ = 0.0;
    * vel_msr_ = 0.0;
    * eff_msr_ = 0.0;
    * pos_cmd_ = 0.0;
    * vel_cmd_ = 0.0;
    * eff_cmd_ = 0.0;

    double int_dist(0.0);
    start = ros::Time::now();
    while( int_dist < disp)
    {
      bool ok = true;
      double dump_factor = dumpFactor();
      if( active_driver_[addr] )
      {
        ok &= driver_->read();
        if( smooth_f) *vel_cmd_  = smooth( ref_vel[addr] , vel_cmd_[0], dump_factor );
        if(!smooth_f) *vel_cmd_  = ref_vel[0];
        int_dist += (*vel_cmd_ * 0.001);
        *pos_cmd_  = *pos_msr_;
        *eff_cmd_  = *eff_msr_;
        ok &= driver_->write();
      }
      
      RT.sleep();
    }
    
    ROS_WARN("wheel_coe Ax. %d finish", addr);
    active_driver_[addr] = false;
    loader_.unloadLibraryForClass("coe_hw_plugins::DS402Complete");
    driver_ = nullptr;
    base_driver_->reset();
    base_driver_.reset( );
  }
  catch ( pluginlib::PluginlibException& ex )
  {
    ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
    throw std::runtime_error( ("Failed in loading driver at address " + std::to_string( addr ) ).c_str() );
  }
  
}

void signalHandler( int signum )
{
  stop_threads = true;
  
//  threads.join_all();
}

int main ( int argc, char* argv[] )
{
  int axe = atoi(argv[1]);
  disp = atof(argv[2]);
  signal(SIGINT, signalHandler);  
//   signal(SIGSEGV, signalHandler);  
//   signal(SIGABRT, signalHandler);  
  
   
  if( !realtime_utilities::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit. Have you launched the node as superuser?");
    return -1;
  }
  realtime_utilities::period_info  pinfo;
  if( !realtime_utilities::rt_init_thread( MY_STACK_SIZE, sched_get_priority_max(SCHED_RR), SCHED_RR, &pinfo, 0.001 * 1e9   ) )
  {
    ROS_FATAL("Failed in setting thread rt properties. Exit. ");
    std::raise(SIGINT);
    return -1;
  }
  
  ros::init ( argc,argv,"eureca_client");
  ros::NodeHandle nh("~");
  
     start = ros::Time::now();

  ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  ros::AsyncSpinner spinner(4);
  spinner.start();

//   coe_target_vel_  = nh.subscribe<geometry_msgs::Twist> ( "/agv/command/velocity", 1, &setCoeTargetVelocity);
  
  boost::thread::attributes coe_main_thread_attr;
  
  coe_main_thread_attr.set_stack_size( PTHREAD_STACK_MIN + MY_STACK_SIZE );

  
  threads.add_thread( new boost::thread(wheel_coe,axe,nh));
  threads.add_thread( new boost::thread(axMainThread));
  
  while( ros::ok() )
  {
    ROS_INFO_THROTTLE(5,"Communication threads are starting..");
    if( active_driver_[0] /*&& active_driver_[1] && active_driver_[2]*/ )
    {
        ROS_INFO_THROTTLE(2,"Communication threads started..");
        break;
    }
    if( stop_threads )
    {
        ROS_INFO_THROTTLE(2,"Stop trhead ...exit...");
        break;
    }
    ros::Duration(0.5).sleep();
  }
  
  if( !stop_threads )
  {
    threads.add_thread( new boost::thread(axMainThread));
  }
      
  while( !stop_threads  )
  {
    if( !active_driver_[0] /*&& !active_driver_[1] && !active_driver_[2] */)
    {
        ROS_INFO_THROTTLE(2,"Communication threads concluded.");
    }
    ros::Duration(0.1).sleep();
  }

//  threads.join_all();
  
  return 1;
}

