
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <signal.h>

#include <ros/ros.h>
 

#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <coe_driver/CoeMessage.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_hw_plugins/coe_ds402_plugin.h>
#include <geometry_msgs/Twist.h>
#include <realtime_tools/realtime_publisher.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>


static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "coe";

//Homing Vars
double rpm2radps( double rpm ) { return rpm * 2.0 * M_PI / 60.0 ; }
  
static const double         ACC_DEC_RAD_S2 = rpm2radps( 0.1 ) ; // Input: RPM/s, Output: rad/s^2

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

bool   active_driver_[3] = {false, false, false};

bool stop_threads = false;

void wheel_coe(int addr,ros::NodeHandle nh)
{

  pluginlib::ClassLoader  <coe_driver::CoeHwPlugin>       loader_("coe_driver", "coe_driver::CoeHwPlugin");
  boost::shared_ptr       <coe_driver::CoeHwPlugin>       base_driver_;
  coe_hw_plugins::DS402Complete*                          driver_;
  
  double* pos_msr_;
  double* vel_msr_;
  double* eff_msr_;
  double* pos_cmd_;
  double* vel_cmd_;
  double* eff_cmd_;
  double int_dist(0.0);
  constexpr double disp[3] = {0.0 , 2.96 , 1.50};
  
  ros::Rate RT(1000);
  
  try
  {
      
    base_driver_ = loader_.createInstance ( "coe_hw_plugins::DS402Complete" );
    driver_ = (coe_hw_plugins::DS402Complete*)base_driver_.get();
  
    if( driver_->initialize ( nh, "/eureca_master/coe/", addr+4) )
    {
      driver_->jointStateHandle  ( &pos_msr_, &vel_msr_, &eff_msr_);
      driver_->jointCommandHandle( &pos_cmd_, &vel_cmd_, &eff_cmd_);
    }
    
    if(!driver_->setTargetState( "MOO_HOMING" ))
    {
        ROS_WARN_STREAM("Set Target State failed...");
        stop_threads = true;
        return;
    }

    coe_core::ds402::status_word_t s = driver_->getStatusWord();
    coe_core::ds402::homing_status_word_t*  sw_ = (coe_core::ds402::homing_status_word_t*)&s;
    

    
    while(!(bool)sw_->bits.homing_attained)
    {
      bool ok_h = true;
      ok_h &= driver_->read();
      s = driver_->getStatusWord();
      sw_ = (coe_core::ds402::homing_status_word_t*)&s;
      ROS_INFO_THROTTLE(1,"state handle %s", coe_core::ds402::to_string ( *sw_, 'b' ).c_str());
      ok_h &= driver_->write();
    }
    
    ros::Duration(0.5).sleep();
    
    ROS_ERROR("FINISHED Homing  : %d",addr);
    
    if(!driver_->setTargetState( "MOO_CYCLIC_SYNCHRONOUS_VELOCITY" ))
    {
      ROS_WARN_STREAM("Set Target State failed...");
      stop_threads = true;
      return;
    }

    
    if(!driver_->setHardRT())
    {
      ROS_WARN_STREAM("Set HardRT failed...");
      stop_threads = true;
      return;
    }
    
    active_driver_[addr] = true;
    
    * pos_msr_ = 0.0;
    * vel_msr_ = 0.0;
    * eff_msr_ = 0.0;
    * pos_cmd_ = 0.0;
    * vel_cmd_ = 0.0;
    * eff_cmd_ = 0.0;

    ros::Time start = ros::Time::now();
    
    while( int_dist < disp[addr])
    {
      bool ok = true;
      if( active_driver_[addr] )
      {
        ok &= driver_->read();
        *vel_cmd_  = 0.2 ;
//        *pos_cmd_ += (*vel_cmd_ * 0.001);
        *pos_cmd_ = *pos_msr_;
        int_dist += (*vel_cmd_ * 0.001);
        *eff_cmd_  = *eff_msr_;
        ok &= driver_->write();
      }
      
      RT.sleep();
    }
    
    ROS_WARN("wheel_coe Ax. %d finish", addr);
    active_driver_[addr] = false;
    loader_.unloadLibraryForClass("coe_hw_plugins::DS402Complete");
    driver_ = nullptr;
    base_driver_->reset();
    base_driver_.reset( );
    
  }
  catch ( pluginlib::PluginlibException& ex )
  {
    ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
    throw std::runtime_error( ("Failed in loading driver at address " + std::to_string( addr ) ).c_str() );
  }

}

void signalHandler( int signum )
{
  stop_threads = true;
}

int main ( int argc, char* argv[] )
{
  signal(SIGINT, signalHandler);  
//   signal(SIGSEGV, signalHandler);  
//   signal(SIGABRT, signalHandler);  
  
  if( !realtime_utilities::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit. Have you launched the node as superuser?");
    return -1;
  }
  realtime_utilities::period_info  pinfo;
  if( !realtime_utilities::rt_init_thread( MY_STACK_SIZE, sched_get_priority_max(SCHED_RR), SCHED_RR, &pinfo, 0.001 * 1e9   ) )
  {
    ROS_FATAL("Failed in setting thread rt properties. Exit. ");
    std::raise(SIGINT);
    return -1;
  }
  
  ros::init ( argc,argv,"eureca_client");
  ros::NodeHandle nh("~");
  
  ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  ros::AsyncSpinner spinner(4);
  spinner.start();
  
  boost::thread::attributes coe_main_thread_attr;
  
  coe_main_thread_attr.set_stack_size( PTHREAD_STACK_MIN + MY_STACK_SIZE );

  boost::thread t1{wheel_coe,2,nh};
  t1.join();
  boost::thread t2{wheel_coe,1,nh};
  t2.join();
  boost::thread t3{wheel_coe,0,nh};
  t3.join();

  return 1;

}

