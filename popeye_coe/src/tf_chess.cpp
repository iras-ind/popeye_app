﻿#include <stdio.h>
#include <chrono>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <boost/thread/condition.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_kdl.h>
#include <std_msgs/Float32.h>
#include <cmath>
#include "flycapture/FlyCapture2.h"
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <eigen3/Eigen/Core>
#include <eigen_conversions/eigen_msg.h>

#include <trac_ik/trac_ik.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>

template <typename T>
class circ_buffer : private boost::noncopyable
{
public:
    typedef boost::mutex::scoped_lock lock;
    circ_buffer() {}
    ~circ_buffer() {}
    circ_buffer(int n, const T* default_value = nullptr )
    {
      cb.set_capacity(n);
      if( default_value )
        for( size_t i=0; i<n;i++)
        {
          push_back( *default_value );
        }

    }
    void push_back (T imdata)
    {
        lock lk(monitor);
        cb.push_back(imdata);
        buffer_not_empty.notify_one();
    }
    void push_front (T imdata)
    {
        lock lk(monitor);
        cb.push_front(imdata);
        buffer_not_empty.notify_one();
    }
    const T& front()
    {
        lock lk(monitor);
        while (cb.empty())
            buffer_not_empty.wait(lk);
        return cb.front();
    }
    const T& back()
    {
        lock lk(monitor);
        while (cb.empty())
            buffer_not_empty.wait(lk);
        return cb.back();
    }

    const T& operator[](unsigned int i)
    {
      lock lk(monitor);
      while (cb.empty())
          buffer_not_empty.wait(lk);
      return cb[i];
    }

    void pop_front()
    {
        lock lk(monitor);
        if(cb.empty())
            return;
        return cb.pop_front();
    }

    void clear()
    {
        lock lk(monitor);
        cb.clear();
    }

    int size()
    {
        lock lk(monitor);
        return cb.size();
    }

    void set_capacity(int capacity)
    {
        lock lk(monitor);
        cb.set_capacity(capacity);
    }

    bool empty()
    {
        lock lk(monitor);
        return cb.empty();
    }

    bool full()
    {
        lock lk(monitor);
        return cb.full();
    }

    boost::circular_buffer<T>& get()
    {
      return cb;
    }

    typename boost::circular_buffer<T>::iterator       begin ( )       { lock lk(monitor); return cb.begin(); }
    typename boost::circular_buffer<T>::iterator       end   ( )       { lock lk(monitor); return cb.end();   }

    typename boost::circular_buffer<T>::const_iterator begin ( ) const { lock lk(monitor); return cb.begin(); }
    typename boost::circular_buffer<T>::const_iterator end   ( ) const { lock lk(monitor); return cb.end();   }

    typename boost::circular_buffer<T>::const_iterator cbegin( ) const { lock lk(monitor); return cb.cbegin();}
    typename boost::circular_buffer<T>::const_iterator cend  ( ) const { lock lk(monitor); return cb.cend();  }


private:
    size_t cnt;
    boost::condition buffer_not_empty;
    boost::mutex monitor;
    boost::circular_buffer<T> cb;
};

using namespace cv;
using namespace std;
using namespace std::chrono;
using namespace FlyCapture2;

int HS ;
int WS ;

circ_buffer<Mat> queue_mat(1);
circ_buffer<tf::StampedTransform> queue_trans(1);

auto start_t = std::chrono::high_resolution_clock::now();;
auto end_t = std::chrono::high_resolution_clock::now();;

Mat frameTh,frameLoc,frameTmp,corners_det;
Mat intrinsics, distortion;

//Moveit Vars
static const std::string PLANNING_GROUP = "popeye_arm";

void read_img_thread(Camera& camera)
{
  camera.StartCapture();
  for(;;)
  {
      // Get the image
      Image rawImage;
      FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );
      if ( error != PGRERROR_OK )
      {
          ROS_DEBUG_THROTTLE(3,"capture error");
          continue;
      }

      // convert to rgb
      Image rgbImage;
      rawImage.Convert( FlyCapture2::PIXEL_FORMAT_MONO8, &rgbImage );

      // convert to OpenCV Mat
      unsigned int rowBytes = (double)rgbImage.GetReceivedDataSize()/(double)rgbImage.GetRows();
      cv::Mat image = cv::Mat(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC1, rgbImage.GetData(),rowBytes);
      queue_mat.push_front(boost::move(image));
  }
}

void tf_thread(vector<Point3f> matPoint,ros::NodeHandle nh)
{
  bool PubImage(false);
  if (!nh.getParam("PubImage",PubImage))
  {
    ROS_ERROR("Impossible to find PubImage\n");
  }
  bool patt;
  Mat rvec(3,1,DataType<double>::type);
  Mat tvec(3,1,DataType<double>::type);
  tf::TransformBroadcaster pose_tf;
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.frame_id = "camera_popeye";
  odom_trans.child_frame_id = "chessboard";
  image_transport::Publisher pub;
  if(PubImage)
  {
    image_transport::ImageTransport it(nh);
    pub = it.advertise("camera/image_calib", 1);
  }
  ros::Rate RT(15);

  for(;;)
  {
    start_t = end_t;
    if (queue_mat.size()!=1) {continue;}
    cv::Matx33d R;
    try
    { frameLoc = queue_mat[0];
      patt =   findChessboardCorners(frameLoc,Size(WS,HS),corners_det,CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE+ CALIB_CB_FAST_CHECK);
      if(patt) cornerSubPix(frameLoc, corners_det, Size((int)(HS/2.0)+1,(int)(WS/2.0)+1), Size(-1, -1),TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
      drawChessboardCorners(frameLoc,Size(WS,HS),corners_det,patt);
      if(!corners_det.empty()) solvePnP(matPoint,corners_det,intrinsics,distortion,rvec,tvec);
      cv::Rodrigues(rvec, R);
    }
    catch(...)
    {
        ROS_DEBUG_THROTTLE(3,"no detection");continue;
    }

    Eigen::Affine3d wRo;
    wRo.linear() << R(0,0), R(0,1), R(0,2), R(1,0), R(1,1), R(1,2), R(2,0), R(2,1), R(2,2);
    wRo.translation() << tvec.at<double>(0), tvec.at<double>(1), tvec.at<double>(2);

    odom_trans.header.stamp = ros::Time::now();
    odom_trans.transform.translation.x = tvec.at<double>(0,0);
    odom_trans.transform.translation.y = tvec.at<double>(0,1);
    odom_trans.transform.translation.z = tvec.at<double>(0,2);
    odom_trans.transform.rotation = tf::createQuaternionMsgFromRollPitchYaw(rvec.at<double>(0,0),rvec.at<double>(0,1),rvec.at<double>(0,2));

    tf::transformEigenToMsg(wRo, odom_trans.transform);

    pose_tf.sendTransform(odom_trans);
    if(PubImage)
    {
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", frameLoc).toImageMsg();
        pub.publish(msg);
    }
    RT.sleep();
    end_t = std::chrono::high_resolution_clock::now();
    }
}

void kinCorrection()
{
  tf::TransformBroadcaster br;
  tf::TransformListener listener;
  float samples(0.0);
  ros::Rate rate(5.0);
  tf::StampedTransform Tcp,Tlink_c;
  std::vector<tf::StampedTransform> transforms;
  bool isset(false);
  while(!isset){
    try
    {
      ROS_WARN("Getting the static tranform");
      listener.lookupTransform("/chess", "/popeye_flange", ros::Time(0), Tcp);
      isset = true;
    }catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(0.2).sleep();
    }
  }
  isset = false;
  while(!isset){
    try
    {
      ROS_WARN("Getting the static tranform 2");
      listener.lookupTransform("/world", "/chess", ros::Time(0), Tlink_c);
      isset = true;
    }catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
      ros::Duration(0.2).sleep();
    }
  }
  ROS_WARN("Passed");
  tf::StampedTransform tmp_trans;
  do
  {
    std::cout << "here sample  : " << samples << std::endl;

    try
    {
      listener.lookupTransform("/world", "/chessboard", ros::Time(0), tmp_trans);
      if(tmp_trans.getOrigin().length() > 0.4 && tmp_trans.getOrigin().length() < 1.3)
      {
//        tmp_trans = tf::StampedTransform(tmp_trans*Tcp,ros::Time::now(),"world","new_tcp");
        std::cout << tmp_trans.getOrigin().getX() << " "  << tmp_trans.getOrigin().getY() << " "  << tmp_trans.getOrigin().getZ() << " " << std::endl;
        br.sendTransform(tmp_trans);
        transforms.push_back(tmp_trans);
        samples++;
      }
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("%s",ex.what());
      ros::Duration(0.5).sleep();
    }
    rate.sleep();
  }while (samples <10);

  ROS_WARN("TF capture finished");

  ROS_WARN("transforms[0] : ");
  std::cout << transforms[0].getOrigin().getX() << "   " << transforms[0].getOrigin().getY() << "   " << transforms[0].getOrigin().getZ() << std::endl << std::endl;

  //TRAC IK

  TRAC_IK::TRAC_IK tracik_solver("world", "chess", "/robot_description", 0.1, 1e-5);
  KDL::Chain chain;
  KDL::JntArray ll, ul;

  bool valid = tracik_solver.getKDLChain(chain);
  if (!valid) {
    ROS_ERROR("There was no valid KDL chain found");
    return;
  }

  valid = tracik_solver.getKDLLimits(ll,ul);

  if (!valid) {
    ROS_ERROR("There were no valid KDL joint limits found");
    return;
  }

  assert(chain.getNrOfJoints() == ll.data.size());
  assert(chain.getNrOfJoints() == ul.data.size());

  KDL::ChainFkSolverPos_recursive fk_solver(chain);
  KDL::ChainIkSolverVel_pinv vik_solver(chain); // PseudoInverse vel solver
  KDL::ChainIkSolverPos_NR_JL kdl_solver(chain,ll,ul,fk_solver, vik_solver, 1, 1e-5);

  KDL::JntArray result;
  KDL::JntArray nominal(chain.getNrOfJoints());
  for (uint j=0; j<nominal.data.size(); j++) {
    nominal(j) = (ll(j)+ul(j))/2.0;
  }

  KDL::Frame end_effector_pose;
  tf::transformTFToKDL(tmp_trans,end_effector_pose);
//  end_effector_pose.p = KDL::Vector(tmp_trans.getOrigin().getX(),tmp_trans.getOrigin().getY(),tmp_trans.getOrigin().getZ());


  int rc = tracik_solver.CartToJnt(nominal,end_effector_pose,result);
  if (rc<0)
    ROS_ERROR("PD no sol");
  std::cout << "Data :: " << std::endl << result.data << std::endl;

  for (uint j=0; j<nominal.data.size(); j++) {
    nominal(j) = (ll(j)+ul(j))/2.0;
  }

  boost::posix_time::ptime start_time;
  boost::posix_time::time_duration diff;
  start_time = boost::posix_time::microsec_clock::local_time();
  double elapsed = 0;

  do {
    nominal=result; // when iterating start with last solution
    rc=kdl_solver.CartToJnt(nominal,end_effector_pose,result);
    diff = boost::posix_time::microsec_clock::local_time() - start_time;
    elapsed = diff.total_nanoseconds() / 1e9;
  } while (elapsed < 0.1);

  if (rc<0)
    ROS_ERROR("PD no sol");

  std::cout << "Data :: " << std::endl << result.data << std::endl;

}


int handleError( int status, const char* func_name,
                 const char* err_msg, const char* file_name,
                 int line, void* userdata )
{
  return 0;
}

int main(int argc, char** argv )
{
  ros::init(argc, argv, "popeye_camera_node");
  ros::NodeHandle nh;
  cv::redirectError(handleError);

  std::string CameraCalibPath;
  int camera_index = 0;
  std::string src_path;
  float square_pix;

  ros::Duration(1).sleep();

  if (!nh.getParam("camera_calib",CameraCalibPath))
  {
    ROS_ERROR("Impossible to find camera_calib\n");
    return -1;
  }

  if (!nh.getParam("camera_index",camera_index))
  {
    ROS_ERROR("Impossible to find camera_index\n");
    return -1;
  }

  if (!nh.getParam("height_edges",HS))
  {
    ROS_ERROR("Impossible to find height_edges\n");
    return -1;
  }

  if (!nh.getParam("width_edges",WS))
  {
    ROS_ERROR("Impossible to find height_edges\n");
    return -1;
  }

  if (!nh.getParam("square_size",square_pix))
  {
    ROS_ERROR("Impossible to find square_size\n");
    return -1;
  }

  cout << "camera info path : " << CameraCalibPath << endl;
  FileStorage fs(CameraCalibPath, FileStorage::READ);
  fs["camera_matrix"] >> intrinsics;
  fs["distortion_coefficients"] >> distortion;
  std::cout << intrinsics << distortion << std::endl;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  FlyCapture2::Error error;
  Camera camera;
  CameraInfo camInfo;

  // Connect the camera
  error = camera.Connect( 0 );
  if ( error != PGRERROR_OK )
  {
      ROS_ERROR("Failed to connect to camera");
  }

  vector<Point3f> matPoint;

  for(int i=1;i<=HS;i++)
  {
    for(int j=1;j<=WS;j++)
    {
      matPoint.push_back(Point3f(float(j)*square_pix - ((float(WS+1)/2.0)*square_pix),float(i)*square_pix - ((float(HS+1)/2.0)*square_pix),0.0));
    }
  }

  boost::thread t_cam(read_img_thread,boost::ref(camera));
  cout << "Camera thread initialized " << endl;
  usleep(2000000);
  boost::thread t_tf(tf_thread,matPoint,nh);
  cout << "TF thread initialized " << endl;
  usleep(2000000);
  cout << "Correction thread initialized " << endl;
  boost::thread corr{kinCorrection};

  ros::waitForShutdown();

  return 0;
}
