#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>
#include <csignal>
#include <signal.h>

#include <ros/ros.h>
 

#include <std_msgs/Float64.h>
#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>
#include <pluginlib/class_loader.h>
#include <realtime_tools/realtime_publisher.h>
#include <coe_driver/CoeMessage.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_hw_plugins/coe_ds402_plugin.h>

const std::vector< int > DRIVER_ADDRESSES = { 1 };

struct EurecaTest
{
  
  struct Axis
  {
    
    ros::NodeHandle nh_;
    const int       addr_;
    uint16_t*       status_word_;
    uint16_t*       control_word_;
    double*         pos_msr_;
    double*         vel_msr_;
    double*         eff_msr_;
    double*         pos_cmd_;
    double*         vel_cmd_;
    double*         eff_cmd_;
    bool            active_driver_;
    
    ros::ServiceServer coe_config_service_  ;
    ros::ServiceServer coe_moo_service_     ;
                                            ;
    ros::ServiceServer coe_status_service_  ;
    ros::ServiceServer coe_errors_service_  ;
                                            ;
    ros::ServiceServer coe_command_service_ ;
    ros::Subscriber    coe_target_pos_      ;
    ros::Subscriber    coe_target_vel_      ;
    ros::Subscriber    coe_target_tau_      ;
    
    std::shared_ptr< realtime_tools::RealtimePublisher< coe_driver::Ds402States > > state_pub_;    
    
    pluginlib::ClassLoader  <coe_driver::CoeHwPlugin>       loader_;
    boost::shared_ptr       <coe_driver::CoeHwPlugin>       base_driver_;
    coe_hw_plugins::DS402Complete*                          driver_;
    
    
    Axis( ros::NodeHandle& nh, const int addr, const std::string& plugin_package, const std::string& plugin_base_name, const std::string& plugin_name, const std::string& param_namespace ) 
      : nh_           (nh     )
      , addr_         (addr   )
      , status_word_  (nullptr)
      , control_word_ (nullptr)
      , pos_msr_      (nullptr)
      , vel_msr_      (nullptr)
      , eff_msr_      (nullptr)
      , pos_cmd_      (nullptr)
      , vel_cmd_      (nullptr)
      , eff_cmd_      (nullptr)
      , active_driver_(false  )
      , loader_       (plugin_package, plugin_base_name)
    {
      try
      {
        base_driver_ = loader_.createInstance ( plugin_name );
        
        driver_ = (coe_hw_plugins::DS402Complete*)base_driver_.get();
        if( driver_->initialize ( nh, param_namespace, addr) )
        {
          status_word_ = driver_->stateHandle ( );
          control_word_ = driver_->controlWordHandle ( );
          driver_->jointStateHandle  ( &pos_msr_, &vel_msr_, &eff_msr_);
          driver_->jointCommandHandle( &pos_cmd_, &vel_cmd_, &eff_cmd_);
        }
      }
      catch ( pluginlib::PluginlibException& ex )
      {
          ROS_ERROR ( "The plugin '%s' of the class '%s' failed to load for some reason (parameters at '%s'). Error: %s", plugin_name.c_str(), plugin_base_name.c_str(), param_namespace.c_str(), ex.what() );
          throw std::runtime_error( ("Failed in loading driver at address " + std::to_string( addr ) ).c_str() );
      }      
      
      coe_config_service_  = nh_.advertiseService             ( "ax"+std::to_string(addr)+"_set_operation_mode", &Axis::setOperationMode,     this);
      coe_moo_service_     = nh_.advertiseService             ( "ax"+std::to_string(addr)+"_get_operation_mode", &Axis::getOperationMode,     this);

      coe_status_service_  = nh_.advertiseService             ( "ax"+std::to_string(addr)+"_get_status_word",    &Axis::getCoeState,          this);

      coe_command_service_ = nh_.advertiseService             ( "ax"+std::to_string(addr)+"_set_coe_command",    &Axis::setCoeCommand,        this);
      coe_target_pos_      = nh_.subscribe<std_msgs::Float64> ( "ax"+std::to_string(addr)+"_target_position", 1, &Axis::setCoeTargetPosition, this);
      coe_target_vel_      = nh_.subscribe<std_msgs::Float64> ( "ax"+std::to_string(addr)+"_target_velocity", 1, &Axis::setCoeTargetVelocity, this);
      coe_target_tau_      = nh_.subscribe<std_msgs::Float64> ( "ax"+std::to_string(addr)+"_target_torque",   1, &Axis::setCoeTargetTorque,   this);
      
      state_pub_.reset( new realtime_tools::RealtimePublisher< coe_driver::Ds402States >  (nh, "ax"+std::to_string(addr)+"_ds402_state", 4) );
      
      active_driver_ = true;
    }
    void reset()
    {
      if( driver_ != nullptr )
      {
        driver_ = nullptr;
        base_driver_->reset();
        base_driver_.reset( );
      }
    }
    ~Axis()
    {
      ROS_INFO("%sReset the plugin%s",BOLDMAGENTA(),RESET());
      reset();
             
      ROS_INFO("%sForce the unload%s",BOLDMAGENTA(),RESET());
      loader_.unloadLibraryForClass("coe_hw_plugins::DS402Complete");
      ROS_INFO("%sDone unload%s",BOLDMAGENTA(),RESET());
     
      ROS_INFO("%sKill services%s",BOLDMAGENTA(),RESET());
      coe_moo_service_.shutdown();
      coe_errors_service_.shutdown();
      coe_status_service_.shutdown();
      coe_config_service_.shutdown();

      ROS_INFO("%sStop Publishers%s",BOLDMAGENTA(),RESET());
      state_pub_->stop();

    }
    
    
    
    bool getCoeState( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
    {
      std::string moo_str = driver_->getActualState( );
      coe_core::ds402::ModeOperationID  moo = coe_core::ds402::to_modeofoperationid( moo_str );
      res.message  += "Mode of Operation Active: " + std::to_string( moo ) + " - " + coe_core::ds402::to_string(moo) + "\n";
      if( moo == coe_core::ds402::MOO_HOMING )
      {
        coe_core::ds402::homing_status_word_t status_word; 
        res.message  += coe_core::ds402::to_string(status_word << driver_->getStatusWord(), 's' );
        
        coe_core::ds402::homing_control_word_t control_word; 
        res.message  += coe_core::ds402::to_string(control_word << driver_->getControlWord(), 's' );
      }
      else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_POSITION )
      {
        coe_core::ds402::cyclic_pos_status_word_t status_word; 
        res.message  += coe_core::ds402::to_string(status_word << driver_->getStatusWord(), 's' );
        
        coe_core::ds402::cyclic_pos_control_word_t control_word = driver_->getControlWord(); 
        res.message  += coe_core::ds402::to_string(control_word, 's' );
      } 
      else if( moo == coe_core::ds402::MOO_CYCLIC_SYNCHRONOUS_VELOCITY )
      {
        coe_core::ds402::cyclic_pos_status_word_t status_word; 
        res.message  += coe_core::ds402::to_string(status_word << driver_->getStatusWord(), 's' );
        
        coe_core::ds402::cyclic_pos_control_word_t control_word = driver_->getControlWord();
        res.message  += coe_core::ds402::to_string(control_word, 's' );
      }
      else
      {
        coe_core::ds402::status_word_t status_word = driver_->getStatusWord();
        res.message  += coe_core::ds402::to_string(status_word, 's' );
        
        coe_core::ds402::control_word_t control_word = driver_->getControlWord(); 
        res.message  += coe_core::ds402::to_string(control_word, 's' );
      }

      std::cout << res.message << std::endl;
      return (res.success = true);
    }
    
    bool setCoeCommand ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
    {
    //
      ROS_INFO ( "%s<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<%s",BOLDCYAN(),RESET());
      ROS_INFO ( "Received the Coe Command '%s', module address: %d", req.message.c_str(), req.addr );
      coe_core::ds402::CommandID      cmd;
      coe_core::ds402::status_word_t  status_word = driver_->getStatusWord();
      coe_core::ds402::StateID        start_state  = coe_core::ds402::to_stateid ( status_word );
      std::cout << "Actual Status: '"<< BOLDYELLOW() << coe_core::ds402::to_string(start_state) << RESET() << "'" << std::endl;
      try
      {
        cmd = coe_core::ds402::to_commandid ( req.message );
        std::cout << "Parsed comand: '" << BOLDYELLOW() << coe_core::ds402::to_string( cmd) << RESET() << std::endl;
      }
      catch ( std::exception& e )
      {
        res.message += "Exception:" + std::string ( e.what() );
        res.message += "----\n";
        res.message += "Req. Command: " + std::string ( req.message ) + "\n";
        res.message +=  coe_core::ds402::echo_feasible_commands ( start_state );
        res.success = false;
        return true;
      }

      try
      {

        if(( coe_core::ds402::get_next_state ( start_state, cmd ) == start_state )
        && ( cmd != coe_core::ds402::CMD_LOOPBACK ) && ( cmd != coe_core::ds402::CMD_SWITCH_ON ) && ( cmd != coe_core::ds402::CMD_COMPLETE_RESET ) )
        {
            res.message  = "Error, the command '" + req.message +"' cannot be applied\n" + coe_core::ds402::echo_feasible_commands ( start_state );
            res.success = true;
        }
        else
        {
          coe_core::ds402::StateID next_state = coe_core::ds402::get_next_state ( start_state, cmd );
          std::cout << "Next Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
          
          coe_core::ds402::control_word_t control_word;
          coe_core::ds402::to_control_word( cmd, (uint16_t*)&control_word );
          std::cout << coe_core::ds402::to_string(control_word,'s') << std::endl;
          driver_->setControlWord( control_word );

          res.message  = "None";
          res.success = true;
          
          ROS_WARN ( "*** Wait for the state transition.." );
          coe_core::ds402::StateID act_state; 
          ros::Rate r(10);
          do 
          {
            r.sleep();
            act_state = coe_core::ds402::to_stateid ( driver_->getStatusWord() );
            if( act_state != start_state )
            {
              std::cout << "------------"<<std::endl;
              std::cout << "Old Status:   '"  << BOLDYELLOW() << coe_core::ds402::to_string(start_state) << RESET() << std::endl;
              std::cout << "Act Status:   '"  << BOLDYELLOW() << coe_core::ds402::to_string(act_state) << RESET() << std::endl;
              std::cout << "what_is_happen: " << coe_core::ds402::what_is_happen( start_state, act_state, 1);
              std::cout << "Next Status:   '" << BOLDYELLOW() << coe_core::ds402::to_string(next_state) << RESET() << std::endl;
              std::cout << "------------"<<std::endl;
            }
            if(  act_state == next_state )
              break;
            
            if( ( act_state == coe_core::ds402::STATE_FAULT ) && (cmd != coe_core::ds402::CMD_FAULT_RESET) ) 
            {
              res.message  += "System failure";
              res.success = false;
              return true;
            }
          }
          while( 1 );
          std::cout << coe_core::ds402::echo_feasible_commands ( act_state  ) <<std::endl;
        }

        ROS_INFO ( "%s>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>%s",BOLDCYAN(),RESET());
        return true;
      }
      catch ( std::exception& e )
      {
        res.message  += "Req. Command: " + std::string ( req.message.data() );
        res.message  += "Exception caught :" + std::string ( e.what() );
        res.success = false;
        return true;
      }
    }
    
    bool setOperationMode ( coe_driver::CoeMessage::Request& req, coe_driver::CoeMessage::Response& res )
    {
    
      ROS_INFO ( "Received the Coe Command '%s%s%s', module address: %s%d%s", BOLDYELLOW(),req.message.c_str(), RESET(), BOLDYELLOW(), req.addr, RESET() );
      std::string                       actual_operation_mode = driver_->getActualState();
      coe_core::ds402::ModeOperationID  actual_moo = coe_core::ds402::to_modeofoperationid( actual_operation_mode );
      ROS_INFO ( "Actual moo '%s%s%s', asked '%s%s%s', module address: %d", BOLDYELLOW(),actual_operation_mode.c_str(), RESET(), BOLDYELLOW(),req.message.c_str(), RESET(), req.addr );
      
      auto ns = driver_->getStateNames();
      
      if( std::find(ns.begin(), ns.end(), req.message) == ns.end() )
      {
        ROS_ERROR ( "Asked mode not available.");
        ROS_INFO  ( "Available modes:");
        for( auto const & s : ns )
          ROS_INFO ( "- %s", s.c_str());
        return false;
      }
      
      try
      {
        res.success = driver_->setTargetState( req.message ); 
        if(!res.success)
          ROS_WARN_STREAM("Set Target State failed...");
        res.success = driver_->setHardRT();
        if(!res.success)
          ROS_WARN_STREAM("Set HardRT failed...");
      }
      catch ( std::exception& e )
      {
          res.message += "Exception:" + std::string ( e.what() );
          res.success = false;
          return true;
      }
      return true;
    }
    
    bool getOperationMode ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
    {
      res.message = driver_->getActualState();
      res.success = true; 
      return true;
    }
    
    void setCoeTargetPosition(const std_msgs::Float64::ConstPtr& msg)
    {
      *pos_cmd_ = msg->data;
      *vel_cmd_ = 0.0;
      *eff_cmd_ = 0.0;
    }
    
    void setCoeTargetVelocity(const std_msgs::Float64::ConstPtr& msg)
    {
      *pos_cmd_ = *pos_msr_;
      *vel_cmd_ = msg->data;
      *eff_cmd_ = 0.0;
    }
    
    void setCoeTargetTorque(const std_msgs::Float64::ConstPtr& msg)
    {
      *pos_cmd_ = *pos_msr_;
      *vel_cmd_ = 0.0;
      *eff_cmd_ = msg->data;
    }
    
    bool update() 
    {
      if( active_driver_ )
      {
        coe_driver::CoeHwPlugin::Error error = driver_->read();
        if( error == coe_driver::CoeHwPlugin::COM_ERROR || error == coe_driver::CoeHwPlugin::EXCEPTION_ERROR )
        {
          return false;
        }
        error = driver_->write();
        if( error == coe_driver::CoeHwPlugin::COM_ERROR || error == coe_driver::CoeHwPlugin::EXCEPTION_ERROR )
        {
          return false;
        }
      }
      return true;
    }

  };

  std::map< int, std::shared_ptr<Axis> > axes_;
  
  EurecaTest( ros::NodeHandle& nh ) 
  {
    for( const auto & i : DRIVER_ADDRESSES )
    {
      axes_[ i ].reset( new Axis( nh, i, "coe_driver", "coe_driver::CoeHwPlugin", "coe_hw_plugins::DS402Complete", "/eureca_master/coe/" ) );
    }
  }
  
  ~EurecaTest() 
  {
    unload();
  }
  
  bool unload( )
  {
    try
    {
        for( auto & axis: axes_) 
          axis.second.reset(); 
        
        axes_.clear();
    }
    catch ( pluginlib::PluginlibException& ex )
    {
        ROS_ERROR ( "The plugin failed to load for some reason. Error: %s", ex.what() );
        return false;
    }
    return true;
  }

};

static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "coe";


EurecaTest* test = nullptr;
bool stop_threads = false;

void axMainThread( int iAx )
{

  realtime_utilities::period_info  pinfo;
  if( !realtime_utilities::rt_init_thread( MY_STACK_SIZE, sched_get_priority_max(SCHED_RR), SCHED_RR, &pinfo, 0.001 * 1e9   ) )
  {
    ROS_FATAL("Failed in setting thread rt properties. Exit. ");
    std::raise(SIGINT);
    return;
  }
  
  ros::WallRate rt(1000);
  while(ros::ok() && !stop_threads )
  {  
    if( !test->axes_[iAx]->update() )
    {
      ROS_FATAL("Update failed");
      test->axes_[iAx]->reset();
      return;
    }
    rt.sleep();
  }
  return;
}


static void sigCalled(int sig)
{
  stop_threads = true;
}

int main ( int argc, char* argv[] )
{
  
  if( !realtime_utilities::rt_main_init(PRE_ALLOCATION_SIZE) )
  {
    perror("Error in rt_main_init. Exit. Have you launched the node as superuser?");
    return -1;
  }
  
  ros::init ( argc,argv,"eureca_client", ros::init_options::NoSigintHandler);
  ros::NodeHandle nh("~");
  
  signal(SIGINT,  &sigCalled);


  ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  
  ros::AsyncSpinner spinner(4);
  spinner.start();
  
  ROS_INFO("\n\n\n%s************************* EURECA CLIENT *****************%s",BOLDGREEN(), RESET());
  test = new EurecaTest( nh );
  
  boost::thread::attributes coe_main_thread_attr;
  
  coe_main_thread_attr.set_stack_size( PTHREAD_STACK_MIN + MY_STACK_SIZE );

  boost::thread_group threads;
  for( std::pair< const int, std::shared_ptr<EurecaTest::Axis> > & axis : test->axes_ )
  {
    threads.add_thread( new boost::thread( axMainThread, axis.first) );
  }
  
  threads.join_all();
  
  delete test;
  
  ROS_INFO("Bye bye");
  return 0;
}


