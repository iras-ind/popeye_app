
#include <iostream>
#include <algorithm>
#include <chrono>
#include <cinttypes>

#include <ros/ros.h>

#include <std_msgs/Float64.h>
#include <configuration_msgs/StartConfiguration.h>
#include <configuration_msgs/StopConfiguration.h>
#include <configuration_msgs/ListConfigurations.h>
#include <geometry_msgs/Twist.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <sensor_msgs/JointState.h>

#include <std_msgs/Int8MultiArray.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Trigger.h>

#include <actionlib/client/simple_action_client.h>
#include <pluginlib/class_loader.h>
#include <coe_core/coe_string_utilities.h>

#include <coe_driver/CoeMessage.h>
#include <coe_driver/CoeErrors.h>
#include <coe_driver/Ds402Command.h>
#include <coe_driver/Ds402States.h>
#include <coe_driver/hw_plugin/coe_hw_base_plugin.h>
#include <coe_hw_plugins/coe_ds402_plugin.h>
#include <realtime_tools/realtime_publisher.h>


#include <tf/tf.h>
#include <tf/transform_broadcaster.h>


class FollowJointTrajctoryController
{
private:

  const std::vector<std::string> joint_names_;
  actionlib::SimpleActionClient< control_msgs::FollowJointTrajectoryAction >* traj_client_;

public:

  FollowJointTrajctoryController(const std::string& ns, const std::vector<std::string>& joint_names )
  : joint_names_( joint_names )
  {
    // tell the action client that we want to spin a thread by default
    traj_client_ = new actionlib::SimpleActionClient< control_msgs::FollowJointTrajectoryAction >(ns, true);

    while(!traj_client_->waitForServer(ros::Duration(5.0))){
      ROS_INFO("Waiting for the joint_trajectory_action server");
    }
  }

  ~FollowJointTrajctoryController()
  {
    delete traj_client_;
  }

  void startTrajectory(const control_msgs::FollowJointTrajectoryGoal& goal)
  {
    // When to start the trajectory: 1s from now
    // goal.trajectory.header.stamp = ros::Time::now() + ros::Duration(1.0);
    traj_client_->sendGoal(goal);
  }

  control_msgs::FollowJointTrajectoryGoal toFollowJointTrajectoryGoal( const std::vector< std::vector<double> >& target_jpos
  , const std::vector<double> st )
  {
    //our goal variable
    control_msgs::FollowJointTrajectoryGoal goal;

    // First, the joint names, which apply to all waypoints
    goal.trajectory.joint_names = joint_names_;

    // We will have two waypoints in this goal trajectory
    goal.trajectory.points.resize( target_jpos.size() );

    // First trajectory point
    // Positions
    for(size_t ind=0; ind<target_jpos.size(); ind++ )
    {
      goal.trajectory.points[ind].positions.clear();
      goal.trajectory.points[ind].positions = target_jpos.at(ind);

      // Velocities
      goal.trajectory.points[ind].velocities.clear();
      goal.trajectory.points[ind].velocities.resize(goal.trajectory.points[ind].positions.size());
      for (size_t j = 0; j < goal.trajectory.points[ind].positions.size(); ++j)
      {
        goal.trajectory.points[ind].velocities[j] = 0.0;
      }
      // To be reached 1 second after starting along the trajectory
      std::cout << ros::Duration(st[ind]) << std::endl;
      goal.trajectory.points[ind].time_from_start = ros::Duration(st[ind]);
    }

    //we are done; return the goal
    return goal;
  }

  //! Returns the current state of the action
  actionlib::SimpleClientGoalState getState()
  {
    return traj_client_->getState();
  }



};

static const char*          SET_SDO_SERVER_NAMESPACE = "/eureca_master/coe/set_sdo";
static const char*          GET_SDO_SERVER_NAMESPACE = "/eureca_master/coe/get_sdo";

static const size_t         PRE_ALLOCATION_SIZE             = 100*1024*1024;
static const size_t         MY_STACK_SIZE                   = 100*1024;
static const std::string    COE_DEVICE_PARAMETER_NAMESPACE  = "coe";

std::map< int, boost::shared_ptr<coe_driver::CoeHwPlugin> > driver_;
std::map< int, boost::shared_ptr<pluginlib::ClassLoader  <coe_driver::CoeHwPlugin> > > loader_;


inline coe_driver::SetSdo::Request toSdoRequstMsg ( const std::string& module_unique_identifier, const coe_core::BaseDataObjectEntry* in)
{
  // ROS_INFO_STREAM( "[" << BOLDMAGENTA() << " PROCESS "<< RESET() << "] Set COB-ID: " << in->to_string() );
  coe_driver::SetSdo::Request req;

  req.index     = in->index();
  req.subindex  = in->subindex();

  req.sdotype   = in->type() == ECT_UNSIGNED8  ? coe_driver::SetSdo::Request::TYPE_U8
                : in->type() == ECT_UNSIGNED16 ? coe_driver::SetSdo::Request::TYPE_U16
                : in->type() == ECT_UNSIGNED32 ? coe_driver::SetSdo::Request::TYPE_U32
                : in->type() == ECT_UNSIGNED64 ? coe_driver::SetSdo::Request::TYPE_U64
                : in->type() == ECT_INTEGER8   ? coe_driver::SetSdo::Request::TYPE_I8
                : in->type() == ECT_INTEGER16  ? coe_driver::SetSdo::Request::TYPE_I16
                : in->type() == ECT_INTEGER32  ? coe_driver::SetSdo::Request::TYPE_I32
                : in->type() == ECT_INTEGER64  ? coe_driver::SetSdo::Request::TYPE_I64
                : 99;

  assert( req.sdotype != 99 );
  coe_driver::toBoostArray(in, req.value) ;
  req.desc      = in->name();
  req.module_id = module_unique_identifier;
  req.timeout   = 0.1;

  return req;
}

enum AXES_ID  { SHOULDER_AXIS = 0, ELBOW_AXIS = 1, WRIST_AXIS = 2, ALL_AXES = 3 };
std::map< AXES_ID, std::string > axes_id =  { {SHOULDER_AXIS,  "ShoulderJoint" }
                                            , {ELBOW_AXIS   ,  "ElbowJoint"    }
                                            , {WRIST_AXIS   ,  "WristJoint"    } };

std::map< AXES_ID, std::string > urdf_joints_id = { {SHOULDER_AXIS,  "popeye_shoulder" }
                                                  , {ELBOW_AXIS   ,  "popeye_elbow"    }
                                                  , {WRIST_AXIS   ,  "popeye_wrist"    } };

std::map< AXES_ID, std::string > module_unique_identifiers =  { {SHOULDER_AXIS,  "ShoulderJoint__4" }
                                                              , {ELBOW_AXIS   ,  "ElbowJoint__5"    }
                                                              , {WRIST_AXIS   ,  "WristJoint__6"    } };

std::map< AXES_ID, std::vector< int16_t > > homing_method_codes =  { {SHOULDER_AXIS,  {1} }
                                                                  , {ELBOW_AXIS   ,  {1,2} }
                                                                  , {WRIST_AXIS   ,  {1} } };

std::map< AXES_ID, std::string > homing_configurations = { {SHOULDER_AXIS,  "homing_shoulder_axis" }
                                                         , {ELBOW_AXIS   ,  "homing_elbow_axis"    }
                                                         , {WRIST_AXIS   ,  "homing_wrist_axis"    } };

std::map< AXES_ID, std::string > position_control_configurations = { { SHOULDER_AXIS, "jpos_interpolated_shoulder_axis" }
                                                                   , { ELBOW_AXIS   , "jpos_interpolated_elbow_axis"    }
                                                                   , { WRIST_AXIS   , "jpos_interpolated_wrist_axis"    }
                                                                   , { ALL_AXES     , "jpos_interpolated"               }  };


std::map< AXES_ID, std::string > planner_ns = { { SHOULDER_AXIS, "/planner_hi_shoulder/thor_prefilter/follow_joint_trajectory" }
                                              , { ELBOW_AXIS   , "/planner_hi_elbow/thor_prefilter/follow_joint_trajectory"    }
                                              , { WRIST_AXIS   , "/planner_hi_wrist/thor_prefilter/follow_joint_trajectory"    }
                                              , { ALL_AXES     , "/planner_hi/thor_prefilter/follow_joint_trajectory"          } };


std::map< AXES_ID, double > rest_positions = { { SHOULDER_AXIS,  0.6 }
                                             , { ELBOW_AXIS   , -0.6 }
                                             , { WRIST_AXIS   ,  0.3 } };

std::map< AXES_ID, double > execution_time = { { SHOULDER_AXIS, 10 }
                                             , { ELBOW_AXIS   , 15 }
                                             , { WRIST_AXIS   , 10 } };

std::vector<AXES_ID> list_of_used_axes = { ELBOW_AXIS, WRIST_AXIS, SHOULDER_AXIS };

std::map< AXES_ID, coe_driver::CoeHwPlugin::OperationModeState > operation_mode_states;
sensor_msgs::JointState joint_states;

coe_driver::CoeHwPlugin::OperationModeState toOperationModeState( int v)
{
  return  v == coe_driver::CoeHwPlugin::OPERATION_MODE_IDLE           ?  coe_driver::CoeHwPlugin::OPERATION_MODE_IDLE
        : v == coe_driver::CoeHwPlugin::OPERATION_MODE_RUNNING        ?  coe_driver::CoeHwPlugin::OPERATION_MODE_RUNNING
        : v == coe_driver::CoeHwPlugin::OPERATION_MODE_GOAL_ACHIEVED  ?  coe_driver::CoeHwPlugin::OPERATION_MODE_GOAL_ACHIEVED
        : coe_driver::CoeHwPlugin::OPERATION_MODE_ERROR;
}

void shoulderOperationModeStateCallback( const std_msgs::Int16& res )
{
  operation_mode_states[ SHOULDER_AXIS ] = toOperationModeState( res.data );
}


void elbowOperationModeStateCallback( const std_msgs::Int16& res )
{
  operation_mode_states[ ELBOW_AXIS ] = toOperationModeState( res.data );
}


void wristOperationModeStateCallback( const std_msgs::Int16& res )
{
  operation_mode_states[ WRIST_AXIS ] = toOperationModeState( res.data );
}

void jointStateCallback( const sensor_msgs::JointState& msg)
{
  joint_states = msg;
}


int main ( int argc, char* argv[] )
{
  
  ros::init ( argc, argv, "hooming_popeye");
  ros::NodeHandle nh("~");
  //ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
  ros::AsyncSpinner spinner(4);
  spinner.start();

  ros::ServiceClient set_sdo = nh.serviceClient<coe_driver::SetSdo>(SET_SDO_SERVER_NAMESPACE);
  if (!set_sdo.waitForExistence(ros::Duration(5)))
  {
    ROS_ERROR("No server found for service %s",set_sdo.getService().c_str());
    return -1;
  }

  ros::ServiceClient start_configuration_srv     = nh.serviceClient<configuration_msgs::StartConfiguration>("/configuration_manager/start_configuration" );
  ros::ServiceClient stop_configuration_srv      = nh.serviceClient<configuration_msgs::StopConfiguration >("/configuration_manager/stop_configuration"  );
  ros::ServiceClient list_controller_service_srv = nh.serviceClient<configuration_msgs::ListConfigurations>("/configuration_manager/list_configurations" );


  ros::Subscriber    joint_state_sub             = nh.subscribe("/joint_states", 1, jointStateCallback);
  ros::Subscriber    shoulder_operation_mode_sub = nh.subscribe("/" + module_unique_identifiers[SHOULDER_AXIS] + "_operation_mode_state", 1, shoulderOperationModeStateCallback);
  ros::Subscriber    elbow_operation_mode_sub    = nh.subscribe("/" + module_unique_identifiers[   ELBOW_AXIS] + "_operation_mode_state", 1,    elbowOperationModeStateCallback);
  ros::Subscriber    wtist_operation_mode_sub    = nh.subscribe("/" + module_unique_identifiers[   WRIST_AXIS] + "_operation_mode_state", 1,    wristOperationModeStateCallback);

  for( auto const & axis_id : list_of_used_axes )
  {
    auto axis_name = axes_id[ axis_id ];

    int16_t input_value;
    ROS_INFO_STREAM( std::endl << std::endl << BOLDGREEN() << "***** Homing " << BOLDYELLOW() << axis_name << BOLDGREEN() <<" Joint ********"  << RESET() << std::endl <<std::endl );

    /**
     * @brief HOMING
     */
    ROS_INFO_STREAM( "1. Homing Configuration Selection" );
    auto homing_method = coe_core::ds402::HOMING_METHOD();
    if( homing_method_codes[axis_id].size() > 1 )
    {
      for( auto m : homing_method_codes[axis_id] )
        ROS_INFO_STREAM( "Allowed Code: " << m );

      ROS_INFO_STREAM( "Select the preferred" );
      std::cin >> input_value;
      ROS_INFO_STREAM( " Selected method " << homing_method_codes[axis_id].front() << "is allowed" );
      homing_method << (uint8_t*)&input_value;
    }
    else
    {
      ROS_INFO_STREAM( " -- only the hardcoded method " << homing_method_codes[axis_id].front() << "is allowed" );
      homing_method << (uint8_t*)&(homing_method_codes[axis_id].front());
    }

    ROS_INFO_STREAM( "Set Homing Mode (through SDO) to module" );
    coe_driver::SetSdo::Request req = toSdoRequstMsg ( module_unique_identifiers[axis_id], (coe_core::BaseDataObjectEntry*)&homing_method);
    coe_driver::SetSdo::Response res;
    if( !set_sdo.call(req,res) )
    {
      ROS_FATAL("Error. Server does not asnwer to the call. ");
      return false;
    }
    if( !res.success )
    {
      ROS_INFO_STREAM( "[" << BOLDRED() << "  FAILED "<< RESET() << "] " );
      return false;
    }

    ROS_INFO_STREAM( "2. Start Homing Procedure" );
    ROS_INFO_STREAM( BOLDGREEN() << "Press enter to conitnue" ); std::cin.get();
    configuration_msgs::StartConfiguration start_configuration;
    start_configuration.request.start_configuration = homing_configurations[axis_id];

    if( !start_configuration_srv.call(start_configuration) )
    {
      ROS_ERROR_STREAM("Error in start the configuration " << homing_configurations[axis_id] );
      return -1;
    }
    ROS_INFO_STREAM( ".... Waiting for homing attained" );
    while( ros::ok() )
    {
      ROS_INFO_STREAM_THROTTLE( 2, "The homing state is " << operation_mode_states[axis_id] );
      if( operation_mode_states[axis_id] == coe_driver::CoeHwPlugin::OPERATION_MODE_GOAL_ACHIEVED)
      {
        ROS_INFO_STREAM( BOLDGREEN() << "   **** HOMING ATTAINED ***   "<< RESET() );
        break;
      }
      ros::Duration(0.5).sleep();
    }
  }


 /**

  */
  configuration_msgs::StartConfiguration start_configuration;

  ROS_INFO_STREAM( std::endl << std::endl << BOLDGREEN() << "***** GO TO REST " << BOLDGREEN() <<" Joint ********"  << RESET() << std::endl <<std::endl );

  ROS_INFO_STREAM( "4. Joint Position Controller Activation (joint_to_velocity_controller)" );
  ROS_INFO_STREAM( BOLDGREEN() << "Press enter to conitnue" ); std::cin.get();
  start_configuration.request.start_configuration = position_control_configurations[ ALL_AXES ];

  if( !start_configuration_srv.call(start_configuration) )
  {
    ROS_ERROR_STREAM("Error in start the configuration " << position_control_configurations[ALL_AXES] );
    return -1;
  }

  ROS_INFO_STREAM( "Movement cycle " );
  ROS_INFO_STREAM( BOLDGREEN() << "Press ente1r to conitnue" ); std::cin.get();
  for( size_t l=0;l<10;l++)
  {

  FollowJointTrajctoryController single_joint_controller( planner_ns[ALL_AXES]
                                                        , { urdf_joints_id[SHOULDER_AXIS], urdf_joints_id[ELBOW_AXIS], urdf_joints_id[WRIST_AXIS] } );

  std::vector< std::vector< double > > positions;
  std::vector< double > time_from_start = {0.0, 10.0};
  
  positions.resize(2);
  positions.at(0) = std::vector<double>{ 0.0, 0.0, 0.0 };
  positions.at(1) = std::vector<double>{ rest_positions[SHOULDER_AXIS], rest_positions[ELBOW_AXIS], rest_positions[WRIST_AXIS] };
  control_msgs::FollowJointTrajectoryGoal goal = single_joint_controller.toFollowJointTrajectoryGoal( positions, time_from_start );

  ROS_INFO_STREAM( "... Send goal o Thor" );
  single_joint_controller.startTrajectory(goal);
  while(ros::ok())
  {
      actionlib::SimpleClientGoalState state = single_joint_controller.getState();
      if( state.isDone() )
      {
          ROS_INFO("Trajectory End!");
          break;
      }
      ros::Duration(0.5).sleep();
      ROS_INFO_THROTTLE(2,"Waiting for trajectory end");
  }

  positions.resize(2);
  positions.at(1) = std::vector<double>{ 0.0, 0.0, 0.0 };
  positions.at(0) = std::vector<double>{ rest_positions[SHOULDER_AXIS], rest_positions[ELBOW_AXIS], rest_positions[WRIST_AXIS] };
  control_msgs::FollowJointTrajectoryGoal back = single_joint_controller.toFollowJointTrajectoryGoal( positions, time_from_start );

  ROS_INFO_STREAM( "... Send goal o Thor" );
  single_joint_controller.startTrajectory(back);
  while(ros::ok())
  {
      actionlib::SimpleClientGoalState state = single_joint_controller.getState();
      if( state.isDone() )
      {
          ROS_INFO("Trajectory End!");
          break;
      }
      ros::Duration(0.5).sleep();
      ROS_INFO_THROTTLE(2,"Waiting for trajectory end");
  }

  }
}

