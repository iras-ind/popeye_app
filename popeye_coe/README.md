![Alt text](media/logo-eureca-3.png)

# EXAMPLE OF MOTOR CONTOLLERS USING COE_DRIVER  #

The package has been developed within the scope of the project [EURECA](www.cleansky-eureca.eu/), funnded from the [Clean Sky](www.cleansky.eu) Joint Undertaking under the [European Union’s Horizon 2020]](https://ec.europa.eu/programmes/horizon2020/)  research and innovation programme under grant agreement nº 738039

> _The EURECA project framework is dedicated to innovate the assembly of aircraft interiors using advanced human-robot collaborative solutions. A pool of devices/frameworks will be deployed for teaming up with human operators in a human-centred assistive environment. The main benefits are the substantial improvement of ergonomics in workloads, the increase in the usability level of assembly actions, the digitalization of procedures with logs and visuals for error prevention through dedicated devices. Solutions are peculiarly designed for addressing both the working conditions and the management of the cabin-cargo installation process, such as limited maneuvering space, limited weight allowed on the cabin floor, reducing lead time and recurring costs. With this aim, EURECA will bring together research advancements spanning across design, manufacturing and control, robotized hardware, and software for the specific use-cases in the cabin-cargo final assembly._



### CanOpen over Ethercat ###

The CanOpen over Ethercat, [CoE](https://www.can-cia.org/fileadmin/resources/documents/proceedings/2005_rostan.pdf) hereafter, is among the most vesatile and used can in the industrial field, thanks to its intrinsic powerful architecture.  The packes should be considered as an extension with planty of utilities of the FOSS project Simple Open EtherCAT Master [SOEM](https://github.com/ros-industrial/ethercat-soem).

Tha package has been designed in order to control the new empowering collaborative robot deployed in EURECA 

![Alt text](media/immagine-progetto-small.jpg)


## popeye_coe ##


The executable can be launched only if there are the motors wired to the PC using a standard ethernet cable.

First, in the first terminal you have to launch:

	user@user-pc:~$ roslaunch popeye_coe coe_master.launch 
	
This is just a launche of the _coe_driver_node_ (the CoE Master) (see in __coe_driver__ for further details) properly configure to move the Eureca's empowering collaborative robot (popeye).

```xml
<launch>
  <param name="use_sim_time" type="bool" value="false" /> 

  <arg name="debug" value="false" />

  <arg name="launch-prefix"   value="gdb -ex run --args" if="$(arg debug)" />
  <arg name="launch-prefix"   value=""         unless="$(arg debug)" />
  
  <node launch-prefix="$(arg launch-prefix)" pkg="coe_driver" name="eureca_master" type="coe_driver_node" output="screen" >
    <rosparam command="load" file="$(find popeye_coe)/cfg/coe_config_driver.yaml"  />
    <rosparam command="load" file="$(find popeye_coe)/cfg/coe_config_elmo.yaml"    />
    <param    name="diagnostic_period_parameter" value="100" />
  </node>
  
  <node pkg="diagnostic_aggregator" type="aggregator_node" name="diag_agg" output="screen" >
    <rosparam command="load"  file="$(find popeye_coe)/cfg/analistics.yaml" />
  </node>
</launch>
```

Then, open a second terminal. Two launcher are provided

	user@user-pc:~$ roslaunch popeye_coe coe_client_standalon.launch 
	
that is a simplified example of a CoE client. Or, 
	
	user@user-pc:~$ roslaunch popeye_coe coe_clien_roscontrollers.launch 
	
that is the complete integration of a CoE client with the [ROS CONTROL](http://wiki.ros.org/ros_control) architecture.
