#ifndef __popeye_assembly__task_manager__
#define __popeye_assembly__task_manager__

#include <popeye_assembly/common.h>
#include <popeye_assembly/motion_planner.h>
#include <popeye_assembly/PickExecuteAction.h>
#include <popeye_assembly/PickPlanAction.h>
#include <popeye_assembly/motion_manager.h>


namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace msm::front;

using namespace msm::front::euml;

namespace popeye_assembly
{
namespace task
{
  
  
  // events
  struct pickPlanRequest
  {
    popeye_assembly::PickPlanGoal pick_place_goal_;
    pickPlanRequest(const popeye_assembly::PickPlanGoal& goal)
    {
      pick_place_goal_=goal;
    }
  };
  
  struct pickExecuteRequest
  {
    popeye_assembly::PickExecuteGoal exec_goal_;
    pickExecuteRequest(const popeye_assembly::PickExecuteGoal& goal)
    {
      exec_goal_=goal;
    }
  };
  
  struct pickCancelRequest
  {
    pickCancelRequest(const popeye_assembly::PickExecuteGoal& goal)
    {
    }
  };

  struct update {};

  // front-end: define the FSM structure 
  struct StateMachineStructure : public msm::front::state_machine_def<StateMachineStructure>
  {
    StateMachineStructure( ros::NodeHandle& nh );

    popeye_assembly::MotionPlanner mp_;
    
    popeye_assembly::PickPlanGoal pick_place_goal_;

    ros::Time   start_time_;
    ros::Time   end_of_last_movement_;

    bool        is_planning_failed_;
    
    bool go_to_error_;
    std::shared_ptr<popeye_assembly::motion::StateMachine > motion_fsm_;
    
    void setMotionManager(std::shared_ptr<popeye_assembly::motion::StateMachine>& motion_fsm)
    {
      motion_fsm_ = motion_fsm;
    }

    PREPARE_LOGGER("TPM")

    TYPEDEF_SIMPLE_STATE(Init  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Idle  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Plan  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Replan, "INFO" )
    TYPEDEF_SIMPLE_STATE(Wait  , "INFO" )
    TYPEDEF_SIMPLE_STATE(Error , "ERROR" )
    
    // the initial state of the player SM. Must be defined
    typedef Init initial_state;
    
    TYPEDEF_SIMPLE_ACTION( StorePlanPlanRequest, pickPlanRequest   , "INFO" )
    TYPEDEF_SIMPLE_ACTION( SendPlan            , none              , "INFO" )

    TYPEDEF_SIMPLE_GUARD ( IsTimeExpired       , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsInError           , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsPlanFailed        , none              , "ERROR" )
    TYPEDEF_SIMPLE_GUARD ( IsPlanSuccess       , none              , "INFO"  )
    TYPEDEF_SIMPLE_GUARD ( IsObjectCorrect     , pickExecuteRequest, "INFO"  )


    typedef StateMachineStructure task_p; // makes transition table cleaner
    std::vector<std::string> state_names =  { StateID_Init
                                            , StateID_Idle
                                            , StateID_Plan
                                            , StateID_Replan
                                            , StateID_Wait
                                            , StateID_Error   };
    
    // Transition table for player
    struct transition_table : mpl::vector<
    //    Start    Event              Next      Action                Guard
    //  +---------+-----------------+---------+----------------------+----------------------+
    Row < Init,    none,               Idle,     none,                 none>,
    Row < Idle,    pickPlanRequest,    Plan,     StorePlanPlanRequest, none>,
    Row < Idle,    none,               Error,    none,                 IsInError>,
    
    Row < Plan,    none,               Replan,   none,                 IsPlanFailed>,
    Row < Plan,    none,               Wait,     none,                 IsPlanSuccess>,
    Row < Plan,    none,               Error,    none,                 IsInError>,
    
    Row < Replan,  none,               Idle,     none,                 IsPlanFailed>,
    Row < Replan,  none,               Wait,     none,                 IsPlanSuccess>,
    Row < Replan,  none,               Error,    none,                 IsInError>,
    
    Row < Wait,    pickExecuteRequest, Idle,     SendPlan,             IsObjectCorrect>,
    Row < Wait,    pickCancelRequest,  Idle,     none,                 none>,
    Row < Wait,    none,               Error,    none,                 IsInError>
    
    //  +---------+-------------+---------+---------------------------+----------------------+
    > {};
    // Replaces the default no-transition response.
    template <class FSM,class Event>
    void no_transition(Event const& e, FSM& f,int state)
    {
      if (f.go_to_error_)
        ROS_ERROR("in error state");
    }
    
    std::map<int32_t, std::shared_ptr<AssemblyStep> >    motion_goals_;
    
  };
  
  // Pick a back-end
  typedef msm::back::state_machine<StateMachineStructure> StateMachine;
  inline std::string getStateName(StateMachine const& p)
  {
    return p.state_names[p.current_state()[0]];
  }

}
}

#endif
