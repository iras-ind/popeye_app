#ifndef __motion_planner__popeye_assembly__
#define __motion_planner__popeye_assembly__

#include <popeye_assembly/common.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/collision_detection/collision_matrix.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

#include <popeye_assembly/PickExecuteAction.h>
#include <popeye_assembly/PickPlanAction.h>
#include <cartesian_trajectory/cartesian_trajectory.h>
#include <moveit_planning_helper/iterative_spline_parameterization.h>
#include <moveit_planning_helper/manage_trajectories.h>

#include <popeye_assembly/attach_object.h>

const std::string ATTACH_OBJECT_SRV = "attach_object";
const std::string RESET_SCENE_SRV   = "reset_scene";

namespace popeye_assembly
{         
  struct AssemblyStep
  {
    static const std::vector< int32_t > sequence()
    {
      static const std::vector< int32_t >& steps = 
          { popeye_assembly::PickPlanGoal::APPROACH_A          
          , popeye_assembly::PickPlanGoal::APPROACH_B
          , popeye_assembly::PickPlanGoal::GRASP              
          , popeye_assembly::PickPlanGoal::GRASP_FINALIZATION 
          , popeye_assembly::PickPlanGoal::CARRY              
          , popeye_assembly::PickPlanGoal::PLACE_A            
          , popeye_assembly::PickPlanGoal::PLACE_B            
          , popeye_assembly::PickPlanGoal::PLACE_FINALIZATION 
          , popeye_assembly::PickPlanGoal::GO_TO_REST };
      return steps;
    }
    static const std::vector< std::string > ids()
    {
      static const std::vector< std::string >& steps =
          { "APPROACH_A"
          , "APPROACH_B"
          , "GRASP"
          , "GRASP_FINALIZATION"
          , "CARRY"
          , "PLACE_A"
          , "PLACE_B"
          , "PLACE_FINALIZATION"
          , "GO_TO_REST" };
      return steps;
    }
    static int32_t prev( int32_t step )
    {
      auto const & steps = sequence();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return ( it == steps.begin() ) ? step : *(it -1 );
    }
    static int32_t next( int32_t step )
    {
      auto const & steps =sequence();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return ( it == steps.end() ) ? step : *(it + 1 );
    }
    static std::string to_string( int32_t step )
    {
      auto const & steps = sequence();
      auto const & id    = ids();
      auto const it = std::find( steps.begin(), steps.end(), step );
      return id.at(std::distance( steps.begin(), it ) );
    }
    const control_msgs::FollowJointTrajectoryGoal goal_;
    const std::string                             controller_configuration_;
    const ros::Duration                           t_;
    AssemblyStep ( const control_msgs::FollowJointTrajectoryGoal&  goal
                 , const ros::Duration&                            t
                 , const std::string&                              controller_configuration )
          : goal_                     ( goal )
          , t_                        ( t )
          , controller_configuration_ ( controller_configuration )
        { }
    AssemblyStep ( const AssemblyStep&  cpy )
          : goal_                     ( cpy.goal_ )
          , t_                        ( cpy.t_ )
          , controller_configuration_ ( cpy.controller_configuration_ )
        { }
  };

  // front-end: define the FSM structure 
  class MotionPlanner 
  {
  private:
    ros::NodeHandle     nh_;
    ros::NodeHandle     root_nh_;
    ros::ServiceClient  attach_object_client_;
    
    popeye_assembly::PickPlanGoal pick_place_goal_;

    std::map<int32_t, std::shared_ptr<AssemblyStep> >               motion_goals_;
    std::map<int32_t, tf::Pose >                                    start_conf_;
    std::map<int32_t, tf::Pose >                                    target_conf_;
    std::map<int32_t, std::vector<double> >                         start_jconf_;
    std::map<int32_t, std::vector<double> >                         target_jconf_;
    std::map<int32_t, double >                                      carried_object_attached_;
    std::map<int32_t, std::string         >                         carried_object_pose_;
    std::map<int32_t, std::string         >                         controller_configuration_;
    std::map<int32_t, collision_detection::AllowedCollisionMatrix > acm_;
    std::map<int32_t, double >                                      execution_time_;
    std::map<int32_t, double >                                      discretization_pts_;
    std::shared_ptr<descartes::CartesianTrajectoryHelper>                                 cart_trj_helper_;


    std::string path_to_mesh_;
    ros::Time  start_time_;
    ros::Time end_of_last_movement_;

    std::string                                           robot_description_;
    moveit::planning_interface::MoveGroupInterfacePtr     move_group_;
    std::shared_ptr<robot_model_loader::RobotModelLoader> robot_model_loader_;
    robot_model::RobotModelPtr                            robot_model_;
    robot_state::RobotStatePtr                            robot_state_;
    std::string                                           robot_tip_frame_;
    planning_scene::PlanningScenePtr                      planning_scene_;
    moveit::planning_interface::PlanningSceneInterface    planning_scene_interface_;
    
    tf::TransformListener listener_;
    tf::TransformBroadcaster br_;
    std::string group_name_;
    std::string tool_frame_name_;
    std::string base_frame_name_;
    std::vector<std::string> robot_jnames_;
    std::vector<double> robot_start_jconf_;
    bool attached_;
    
    
    
    bool attachHatbox( const std::string& link_to_attach );

  public:
    
    MotionPlanner( ros::NodeHandle& nh );

    bool init();
    bool planPrepare    ( const popeye_assembly::PickPlanGoal& pick_place_goal );
    bool planComputation( const std::vector<int32_t>& steps
                        , std::map<int32_t, std::shared_ptr<AssemblyStep> >& motion_goals );
    void move_group();
    
  };
  

}

#endif
