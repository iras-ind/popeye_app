#ifndef pickplace__201809071025
#define pickplace__201809071025

#include <popeye_assembly/common.h>

#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>
#include <popeye_assembly/motion_planner.h>

namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace msm::front;

using namespace msm::front::euml;

namespace popeye_assembly
{
namespace motion
{

  
  // events
  struct pickRequest
  {
    const std::map<int32_t,std::shared_ptr<::popeye_assembly::AssemblyStep> > goals_;
    pickRequest ( const std::map<int32_t, std::shared_ptr<::popeye_assembly::AssemblyStep> >& goals ) : goals_ ( goals )       { }
    pickRequest ( const pickRequest& cpy   ) : goals_ ( cpy.goals_ )  { }
  };

  struct update {};
  struct EventNone {};

  /**
   *
   * front-end: define the FSM structure
   *
   *
   */
  struct StateMachineStructure : public msm::front::state_machine_def<StateMachineStructure>
  {
    StateMachineStructure( ros::NodeHandle& nh  ) : nh_( nh ) {}

    bool go_to_error_;
    ros::NodeHandle nh_;

    std::map<int32_t, std::shared_ptr< popeye_assembly::AssemblyStep > > trajectories_;
    ros::Time start_goal_;

    std::shared_ptr< actionlib::SimpleActionClient<control_msgs::FollowJointTrajectoryAction> > trj_action_;
    tf::TransformListener     listener_;
    tf::StampedTransform      tool_transform_;
    std::string               object_frame_;

    PREPARE_LOGGER( "MPM" )

    TYPEDEF_SIMPLE_STATE( Error            , "ERROR" )
    TYPEDEF_SIMPLE_STATE( Init             , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Rest             , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Ready            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( ApproachA        , "INFO"  )
    TYPEDEF_SIMPLE_STATE( ApproachB        , "INFO"  )
    TYPEDEF_SIMPLE_STATE( Grasp            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( CloseGripper     , "INFO"  )
    TYPEDEF_SIMPLE_STATE( GraspFinalization, "INFO"  )
    TYPEDEF_SIMPLE_STATE( Carry            , "INFO"  )
    TYPEDEF_SIMPLE_STATE( PlaceA           , "INFO"  )
    TYPEDEF_SIMPLE_STATE( PlaceB           , "INFO"  )
    TYPEDEF_SIMPLE_STATE( PlaceFinalization, "INFO"  )
    TYPEDEF_SIMPLE_STATE( OpenGripper      , "INFO"  )

    TYPEDEF_SIMPLE_ACTION( CachePlan             , pickRequest, "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartApproachA        , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartApproachB        , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartGrasp            , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( TriggerCloseGripper   , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartGraspFinalization, update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartCarry            , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartPlaceA           , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( StartPlaceB           , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( FinalizePlace         , update     , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( TriggerOpenGripper    , none       , "INFO"  )
    TYPEDEF_SIMPLE_ACTION( GoToRest              , none       , "INFO"  )

    TYPEDEF_SIMPLE_GUARD( IsMovementFinished     , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsGrasped              , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsPlaced               , none        , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsGraspingFinished     , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsPickingFinished      , update      , "INFO" )
    TYPEDEF_SIMPLE_GUARD( IsInError              , update      , "INFO" )

    // the initial state of the player SM. Must be defined
    typedef Init initial_state;
    typedef StateMachineStructure p; // makes transition table cleaner
    std::vector<std::string>  state_names = { StateID_Error
                                            , StateID_Init
                                            , StateID_Rest
                                            , StateID_Ready
                                            , StateID_ApproachA
                                            , StateID_ApproachB
                                            , StateID_Grasp
                                            , StateID_CloseGripper
                                            , StateID_GraspFinalization
                                            , StateID_Carry
                                            , StateID_PlaceA
                                            , StateID_PlaceB
                                            , StateID_PlaceFinalization
                                            , StateID_OpenGripper };

    // Transition table NOTE: the transition table is checked from the bottom to the top
    struct transition_table : mpl::vector<
    //    Start             Event         Next                  Action                        Guard
    //  +----------        +--------------+------------------+---------------------   +----------------------+
    Row < Init             , none         , Rest             ,  none                  , none                   >,
    Row < Init             , update       , Error            ,  none                  , IsInError              >,
    Row < Rest             , pickRequest  , Ready            ,  CachePlan             , none                   >,
    Row < Rest             , update       , Error            ,  none                  , IsInError              >,

    Row < Ready            , none         , ApproachA        ,  StartApproachA        , none                   >,
    Row < Ready            , update       , Error            ,  none                  , IsInError              >,
    
    Row < ApproachA        , none         , ApproachB        ,  StartApproachB        , none                   >,
    Row < ApproachA        , update       , Error            ,  none                  , IsInError              >,

    Row < ApproachB        , update       , Grasp            ,  StartGrasp            , IsMovementFinished     >,
    Row < ApproachB        , update       , Error            ,  none                  , IsInError              >,

    Row < Grasp            , update       , CloseGripper     ,  TriggerCloseGripper   , none                   >,
    Row < Grasp            , update       , Error            ,  none                  , IsInError              >,

    Row < CloseGripper     , update       , GraspFinalization,  StartGraspFinalization, IsGrasped              >,
    Row < CloseGripper     , update       , Error            ,  none                  , IsInError              >,

    Row < GraspFinalization, none         , Carry            ,  StartCarry            , none                   >,
    Row < GraspFinalization, update       , Error            ,  none                  , IsInError              >,

    Row < Carry            , update       , PlaceA           ,  StartPlaceA           , IsMovementFinished     >,
    Row < Carry            , update       , Error            ,  none                  , IsInError              >,

    Row < PlaceA           , update       , PlaceB           ,  StartPlaceB           , IsMovementFinished     >,
    Row < PlaceA           , update       , Error            ,  none                  , IsInError              >,
    
    Row < PlaceB           , update       , PlaceFinalization,  FinalizePlace         , IsMovementFinished     >,
    Row < PlaceB           , update       , Error            ,  none                  , IsInError              >,

    Row < PlaceFinalization, none         , OpenGripper      ,  TriggerOpenGripper    , IsPlaced               >,
    Row < PlaceFinalization, update       , Error            ,  none                  , IsInError              >,

    Row < OpenGripper      , none         , Rest             ,  GoToRest              , none                   >,
    Row < OpenGripper      , update       , Error            ,  none                  , IsInError              >
    > {};

    // Replaces the default no-transition response.
    template <class FSM,class Event>
    void no_transition(Event const& e, FSM& f,int state)
    {
      if (f.go_to_error_)
        ROS_ERROR("in error state");
    }
  };

  // Pick a back-end
  typedef msm::back::state_machine<StateMachineStructure> StateMachine;

  //
  // Testing utilities.
  //
  inline std::string getStateName(StateMachine const& p)
  {
    return p.state_names[p.current_state()[0]];
  }

}
}

#endif
