#!/bin/bash

#At time 0.000
#- Translation: [-0.000, -0.850, 0.770]
#- Rotation: in Quaternion [0.000, -0.383, 0.924, -0.000]
#            in RPY (radian) [-0.785, 0.000, -3.142]
#            in RPY (degree) [-45.000, 0.000, -180.000]


rosservice call --wait /eureca_popeye_scene_configurator/configure_scene "hatbox_pose:
  position:
    x:  0.000
    y: -0.850
    z:  0.770
  orientation:
    x:  0.0
    y: -0.383
    z:  0.924
    w:  0.0
window_pose:
  position:
    x: 0.0
    y: 0.0
    z: 0.0
  orientation:
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0" &

exec "$@"

