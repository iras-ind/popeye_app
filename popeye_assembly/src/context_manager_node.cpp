#include <thread>
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
// MoveIt!
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/kinematic_constraints/utils.h>
#include <rosparam_utilities/rosparam_utilities.h>

#include <moveit_planning_helper/manage_planning_scene.h>
#include <eigen3/Eigen/Core>
#include <tf_conversions/tf_eigen.h>

#include <geometry_msgs/Pose.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_msgs/AttachedCollisionObject.h>

#include <popeye_assembly/configure_planning_scene.h>
#include <popeye_assembly/attach_object.h>

#include <tf/transform_listener.h>
#include <std_srvs/Trigger.h>


const std::string PATH_TO_MESH_NS                        = "package://eureca_description/meshes/components";
const std::string ROBOT_DESCRIPTION_NS                   = "robot_description";
const std::string HATBOX_OBJECT_ID                       = "hatbox";
const std::string HATBOX_CART_OBJECT_ID                  = "hatbox_cart";
const std::string HATBOX_IN_HABOX_CART_FRAME_NS          = "hatbox_in_habox_cart_frame";
const std::string HATBOX_APPROACH_A_IN_HABOX_FRAME_NS    = "hatbox_approach_A_in_hatbox_frame";
const std::string HATBOX_APPROACH_B_IN_HABOX_FRAME_NS    = "hatbox_approach_B_in_hatbox_frame";
const std::string HATBOX_BRACKET_IN_HABOX_FRAME_NS       = "hatbox_bracket_in_hatbox_frame";
const std::string HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS = "hatbox_post_grasping_z_displacement";
const std::string WINDOW_TO_ATTACH_POSE_NS               = "window_to_attach_pose";
const std::string CONFIGURE_SCENE_SRV                    = "configure_scene";
const std::string RESET_SCENE_SRV                        = "reset_scene";
const std::string ATTACH_OBJECT_SRV                      = "attach_object";
const std::string PLANNING_SCENE_TOPIC_NS                = "planning_scene";
const std::string GROUP_NAME_NS                          = "group_name";

// ---------------------------------------------------------------------
bool lookupTransform( const std::string& base_frame, const std::string& target_frame, tf::StampedTransform& tf_transform)\
{
  tf::TransformListener listener;
  ros::Time st = ros::Time::now();
  while( ros::ok() )
  {
    try
    {
      listener.lookupTransform( base_frame, target_frame, ros::Time(0), tf_transform);
      break;
    }
    catch (tf::TransformException ex)
    {
      ROS_WARN_THROTTLE(5,"%s",ex.what());
    }
    if( (ros::Time::now() - st).toSec() > 20.0 )
    {
      ROS_ERROR_THROTTLE(5,"The tf listener didn't found any connections in 20 sec");
      return false;
    }
  }
  return true;
}


bool getParam( ros::NodeHandle& nh, const std::string key, tf::Pose& pose)
{
  XmlRpc::XmlRpcValue config;
  if(!nh.getParam(key,config))
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "' not found!");
    return false;
  }

  std::vector<double> xyz;
  if( !rosparam_utilities::getParamVector( nh, key + "/xyz", xyz ) )
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "'/xyz' is malformed. " );
    return false;
  }
  std::vector<double> rpy;
  if( !rosparam_utilities::getParamVector( nh, key + "/rpy", rpy ) )
  {
    ROS_FATAL_STREAM("Parameter '" << nh.getNamespace() << "/" << key << "'/rpy' is malformed. " );
    return false;
  }

  Eigen::Affine3d value;
  value = Eigen::Affine3d::Identity();
  value = Eigen::AngleAxisd(rpy[2], Eigen::Vector3d::UnitZ() )
        * Eigen::AngleAxisd(rpy[1], Eigen::Vector3d::UnitY() )
        * Eigen::AngleAxisd(rpy[0], Eigen::Vector3d::UnitX() );
  value.translation() = Eigen::Vector3d(xyz[0],xyz[1],xyz[2]);

  tf::transformEigenToTF( value, pose );
  return true;
}

class PlanningSceneConfigurator
{
  ros::NodeHandle nh_;
  std::shared_ptr< robot_model_loader::RobotModelLoader >  robot_model_loader_;
  robot_model::RobotModelPtr                               robot_model_;
  std::string                                              group_name_;
  std::string                                              robot_description_;
  std::string                                              robot_tip_;
  moveit::planning_interface::PlanningSceneInterface       planning_scene_interface_;
  double                                                   z_displacement_;

  ros::ServiceServer                                       attach_srv_;
  ros::ServiceServer                                       configuration_srv_;
  ros::ServiceServer                                       reset_srv_;

  tf::Pose    T_0_h_   ;   // from world  to hatbox
  tf::Pose    T_0_hb_  ;  // from world  to hatbox_bracket
  tf::Pose    T_0_haA_ ;  // from world  to hatbox_approach
  tf::Pose    T_0_haB_ ;  // from world  to hatbox_approach
  tf::Pose    T_0_hc_  ;  // from world  to hatbox_cart
  tf::Pose    T_hc_h_  ;  // from hatbox to hatbox_cart
  tf::Pose    T_h_haA_ ;  // from hatbox to hatbox_approach
  tf::Pose    T_h_haB_ ;  // from hatbox to hatbox_approach
  tf::Pose    T_h_hb_  ;  // from hatbox to hatbox_bracket
  tf::Pose    T_0_he_  ;  // habox escape frame

  bool                             join_thread_;
  tf::TransformBroadcaster         br_;
  std::shared_ptr< std::thread >   br_thread_;

  void threadTFBroadcaster( )
  {
    ros::Rate rt(100);

    bool first_cycle = true;
    do 
    {
      ros::Time t = ros::Time::now();
      std::map< std::string, geometry_msgs::Pose > hatbox_pose = planning_scene_interface_.getObjectPoses( {HATBOX_OBJECT_ID, HATBOX_CART_OBJECT_ID});

      if( hatbox_pose.count( HATBOX_OBJECT_ID ) )
      {
        tf::poseMsgToTF( hatbox_pose[HATBOX_OBJECT_ID ] , T_0_h_);
      }
      else
      {
        std::map<std::string, moveit_msgs::AttachedCollisionObject> aco = planning_scene_interface_.getAttachedObjects( {HATBOX_OBJECT_ID } );
        if( aco.count( HATBOX_OBJECT_ID ) )
        {
          tf::poseMsgToTF( aco[HATBOX_OBJECT_ID ].object.mesh_poses.front(), T_0_h_);
        }
        else
        {
          std::map<std::string, moveit_msgs::CollisionObject> co = planning_scene_interface_.getObjects( {HATBOX_OBJECT_ID } );
          if( co.count( HATBOX_OBJECT_ID ) )
          {
            tf::poseMsgToTF( aco[HATBOX_OBJECT_ID ].object.mesh_poses.front(), T_0_h_);
          }
        }
      }
     
      T_0_hb_  = T_0_h_ * T_h_hb_;
      T_0_haA_ = T_0_h_ * T_h_haA_;
      T_0_haB_ = T_0_h_ * T_h_haB_;
      
      if( first_cycle )
      { 
        tf::poseMsgToTF( hatbox_pose[HATBOX_CART_OBJECT_ID] , T_0_hc_);
        
        T_0_he_  = T_0_h_; 
        T_0_he_.getOrigin() += tf::Vector3(0,0,z_displacement_);
      }
      
      br_.sendTransform(tf::StampedTransform(T_0_h_  , t, "world", "hatbox"));
      br_.sendTransform(tf::StampedTransform(T_0_hb_ , t, "world", "hatbox_bracket"));
      br_.sendTransform(tf::StampedTransform(T_0_haA_, t, "world", "hatbox_approach_A"));
      br_.sendTransform(tf::StampedTransform(T_0_haB_, t, "world", "hatbox_approach_B"));
      br_.sendTransform(tf::StampedTransform(T_0_hc_ , t, "world", "hatbox_cart"));
      br_.sendTransform(tf::StampedTransform(T_0_he_ , t, "world", "hatbox_escape"));
      
      first_cycle = false;
      rt.sleep();
    } while(ros::ok() && !join_thread_  );

  }

  bool configureScene( popeye_assembly::configure_planning_scene::Request&   req
                     , popeye_assembly::configure_planning_scene::Response&  res )
  {
    std::vector< moveit_msgs::CollisionObject > objs;

    tf::poseMsgToTF( req.hatbox_pose, T_0_h_ );
    T_0_hc_ = T_0_h_ * T_hc_h_.inverse();

    tf::poseMsgToTF( req.hatbox_pose, T_0_h_ );
    objs.push_back( *moveit_planning_helper::toCollisionObject (  HATBOX_OBJECT_ID
                                                                , PATH_TO_MESH_NS + "/collision/hatbox.stl"
                                                                , "world"
                                                                , T_0_h_
                                                                , Eigen::Vector3d(1,1,1) ) );

    tf::Pose T_0_hc = T_0_h_ * T_hc_h_.inverse();

    objs.push_back( *moveit_planning_helper::toCollisionObject  ( HATBOX_CART_OBJECT_ID
                                                                , PATH_TO_MESH_NS + "/collision/hatbox_cart.stl"
                                                                , "world"
                                                                , T_0_hc
                                                                , Eigen::Vector3d(1,1,1) ) );

    moveit_msgs::ObjectColor color_hatbox     ; color_hatbox      .id=HATBOX_OBJECT_ID     ; color_hatbox      .color.r = 255; color_hatbox      .color.g = 255; color_hatbox      .color.b = 255; color_hatbox     .color.a = 255;
    moveit_msgs::ObjectColor color_hatbox_cart; color_hatbox_cart .id=HATBOX_CART_OBJECT_ID; color_hatbox_cart .color.r = 120; color_hatbox_cart .color.g = 120; color_hatbox_cart .color.b = 120; color_hatbox_cart.color.a = 255;

    if( !moveit_planning_helper::applyAndCheckPS( ros::NodeHandle()
                                                , objs
                                                , {color_hatbox, color_hatbox_cart}
                                                , ros::Duration(10) ) )
    {
      ROS_FATAL_STREAM("Failed in uploading the collision objects");
      res.success = false;
    }
    else
    {
      ROS_INFO_STREAM("Ok! Objects loaded in the planning scene");
      res.success = true;
    }


    if( br_thread_ == nullptr )
    {
      join_thread_ = false;
      br_thread_.reset( new std::thread( &PlanningSceneConfigurator::threadTFBroadcaster, this ) );
    }

    return true;
  }
  
  
  bool resetScene( std_srvs::Trigger::Request&   req
                 , std_srvs::Trigger::Response&  res )
  {
    moveit::planning_interface::PlanningSceneInterface psi;
    std::map<std::string, moveit_msgs::AttachedCollisionObject> aobjs = planning_scene_interface_.getAttachedObjects( {HATBOX_OBJECT_ID } );
    for( auto & obj  : aobjs )
    {
      moveit_msgs::AttachedCollisionObject& detach_object = obj.second;
      detach_object.object.operation    = detach_object.object.REMOVE; 
      if( !psi.applyAttachedCollisionObject( detach_object ) )
      {
        ROS_ERROR("Failed in detaching the collision object");
        res.success = false;
        return true;
      }
    }
    
    std::map<std::string, moveit_msgs::CollisionObject> cobjs = planning_scene_interface_.getObjects( {HATBOX_OBJECT_ID } );
    for( auto & obj  : cobjs )
    {
      moveit_msgs::CollisionObject& remove_object = obj.second;
      remove_object.operation = remove_object.REMOVE; 
      if( !psi.applyCollisionObject( remove_object ) )
      {
        ROS_ERROR("Failed in detaching the collision object");
        res.success = false;
        return true;
      }
    }
    ros::Duration(2.0).sleep();
    return (res.success = true); 
    
  }
  
  bool attachObject ( popeye_assembly::attach_object::Request&   req
                    , popeye_assembly::attach_object::Response&  res )
  {
    if( req.object_id != HATBOX_OBJECT_ID )
    {
      ROS_ERROR("The object' %s' is not yet supported", req.object_id.c_str() );
      res.success = false;
      return true;
    }
    
    moveit::planning_interface::PlanningSceneInterface psi;

    if( req.link_to_attach == "hatbox_clamper" )
    {

      std::map<std::string, moveit_msgs::CollisionObject> objs = psi.getObjects( { req.object_id } );
      if( objs.size() == 0 )
      {
        ROS_ERROR("No objects in the scene. Is that correct?");
        res.success = false;
        return true;
      }
      
      tf::StampedTransform T_g_h;
      if(!lookupTransform( req.link_to_attach, req.object_id, T_g_h ) )
      {
        ROS_ERROR("No know trasformation from '%s' to'%s'", req.link_to_attach.c_str() , req.object_id.c_str() );
        res.success = false;
        return true;
      }
      
      moveit_msgs::AttachedCollisionObject attached_object;
      attached_object.link_name   = req.link_to_attach;
      attached_object.object      = *moveit_planning_helper::toCollisionObject (  HATBOX_OBJECT_ID, PATH_TO_MESH_NS + "/collision/hatbox.stl", req.link_to_attach, T_g_h, Eigen::Vector3d(1,1,1) ) ;
      attached_object.touch_links = std::vector<std::string>{ req.link_to_attach };

      moveit_msgs::CollisionObject remove_object;
      remove_object.id = req.object_id;
      remove_object.operation = remove_object.REMOVE;
      if(!psi.applyCollisionObject( remove_object ) )
      {
        ROS_ERROR("Failed in removing the collision object");
        res.success = false;
        return true;
      }

      
      if( !psi.applyAttachedCollisionObject( attached_object ) )
      {
        ROS_ERROR("Failed in attaching the collision object");
        res.success = false;
        return true;
      }
    }
    else if( req.link_to_attach == "fuselage_hatbox_final_1" )
    {
      std::map<std::string, moveit_msgs::AttachedCollisionObject> objs = planning_scene_interface_.getAttachedObjects( {HATBOX_OBJECT_ID } );
      if( objs.count( req.object_id ) == 0 )
      {
        ROS_ERROR("No Attached Objects in the scene. Is that correct?");
        res.success = false;
        return true;
      }
      
      //-----------------------------------------------------------------------------
      tf::StampedTransform T_0_h;
      if(!lookupTransform( "/world", req.object_id, T_0_h ) )
      {
        ROS_ERROR("No know trasformation from '%s' to'%s'", req.link_to_attach.c_str() , req.object_id.c_str() );
        res.success = false;
        return true;
      }
      tf::Pose tmp = T_0_h;
      ROS_INFO_STREAM( "Hatbox pose in world: " << moveit_planning_helper::to_string( tmp ) );
      
      moveit_msgs::AttachedCollisionObject detach_object;
      detach_object.link_name           = req.link_to_attach;
      detach_object.object.operation    = detach_object.object.REMOVE;
      detach_object.object.id           = HATBOX_OBJECT_ID;
      
      if( !psi.applyAttachedCollisionObject( detach_object ) )
      {
        ROS_ERROR("Failed in detaching the collision object");
        res.success = false;
        return true;
      }
      
      ros::Time st = ros::Time::now();
      while( ros::ok() )
      {
        objs = planning_scene_interface_.getAttachedObjects( {HATBOX_OBJECT_ID } );
        if( objs.count( HATBOX_OBJECT_ID ) == 0 )
        {
          break;
        }
        if( (ros::Time::now()-st).toSec() > 5 )
        {
          ROS_ERROR_THROTTLE(5,"The '%s' is still in the scene ... waiting Timeout Expired. Abort.", HATBOX_OBJECT_ID.c_str() );
          res.success = false;
          return true;
        }
        ROS_INFO_THROTTLE(5,"The '%s' is still in the scene ... waiting for the complete detach", HATBOX_OBJECT_ID.c_str() );
        ros::Duration(1.0).sleep();
      }
      
      moveit_msgs::CollisionObject add_object = *moveit_planning_helper::toCollisionObject (  HATBOX_OBJECT_ID, PATH_TO_MESH_NS + "/collision/hatbox.stl", "/world", T_0_h, Eigen::Vector3d(1,1,1) ) ;
      if(!psi.applyCollisionObject( add_object ) )
      {
        ROS_ERROR("Failed in removing the collision object");
        res.success = false;
        return true;
      }  
    }
    else
    {
      ROS_ERROR("UFFFFFFFFFFFFFFAAAAAAAAAAA: %s", req.link_to_attach.c_str() );
    }
    ros::Duration(2.0).sleep();
    return (res.success = true);
  }

public:
  PlanningSceneConfigurator(  )
    : nh_  ( "~" )
  {
      if( !getParam( nh_, HATBOX_APPROACH_A_IN_HABOX_FRAME_NS, T_h_haA_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }
      if( !getParam( nh_, HATBOX_APPROACH_B_IN_HABOX_FRAME_NS, T_h_haB_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }


      if( !getParam( nh_, HATBOX_IN_HABOX_CART_FRAME_NS, T_hc_h_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }


      if( !getParam( nh_, HATBOX_BRACKET_IN_HABOX_FRAME_NS, T_h_hb_ ) )
      {
        throw std::runtime_error("Error in extracting param. Abort" );
      }
      
      if( !nh_.getParam( HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS, z_displacement_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<HATBOX_POST_GRASPING_Z_DISPLACEMENT_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      
      if( !nh_.getParam( GROUP_NAME_NS, group_name_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<GROUP_NAME_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      if( !nh_.getParam( ROBOT_DESCRIPTION_NS, robot_description_ ) )
      {
        std::stringstream str; str << "Error in extracting param '" << nh_.getNamespace() <<"/"<<ROBOT_DESCRIPTION_NS<< "'. Abort.";
        throw std::runtime_error( str.str().c_str() );
      }
      
      robot_model_loader_.reset( new robot_model_loader::RobotModelLoader ( robot_description_  ) );
      robot_model_                = robot_model_loader_->getModel();
      configuration_srv_          = nh_.advertiseService(CONFIGURE_SCENE_SRV, &PlanningSceneConfigurator::configureScene, this);
      reset_srv_                  = nh_.advertiseService(RESET_SCENE_SRV    , &PlanningSceneConfigurator::resetScene    , this);
      attach_srv_                 = nh_.advertiseService(ATTACH_OBJECT_SRV  , &PlanningSceneConfigurator::attachObject  , this);
      
      
      robot_tip_ = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getTipFrame();

      ROS_INFO_STREAM("Robot Tip: " << robot_tip_ );
      
//      if( !getParam( nh_, WINDOW_TO_ATTACH_POSE_NS, T_hc_h_ ) )
//      {
//        throw std::runtime_error("Error in extracting param. Abort" );
//      }
      br_thread_  = nullptr;

  }

  ~PlanningSceneConfigurator()
  {
    join_thread_ = true;
    br_thread_->join();
  }

};

int main(int argc, char** argv)
{

  ros::init(argc,argv,"popeye_assembly_test");
  ros::AsyncSpinner spinner(1);
  spinner.start();

  PlanningSceneConfigurator planning_scene_configurator;

  ros::waitForShutdown();
  return 0;
}
