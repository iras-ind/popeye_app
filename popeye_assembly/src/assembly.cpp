
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

#include <moveit/robot_state/conversions.h>

#include <actionlib/client/simple_action_client.h>
#include <moveit_msgs/ExecuteTrajectoryAction.h>
#include <boost/lexical_cast.hpp>
#include <std_msgs/Int64.h>
#include <std_srvs/Empty.h>
#include <std_srvs/Trigger.h>

#include <moveit_planning_helper/manage_trajectories.h>
#include <configuration_msgs/ListConfigurations.h>
#include <configuration_msgs/StartConfiguration.h>
#include <moveit_planning_helper/manage_planning_scene.h>

bool setConfiguration( ros::ServiceClient& client, const std::string& configuration, const double sleep = 1.0 )
{
  configuration_msgs::StartConfiguration srv;
  
  srv.request.start_configuration = configuration;
  srv.request.strictness          = 1;
  
  if (client.call(srv))
  {
    if( srv.response.ok) 
    {
      ROS_INFO("Request accepted.");
      return true;
    }
  }
  ros::Duration(sleep).sleep();
  ROS_ERROR("Failed to call service '%s', configuration requested: %s", client.getService().c_str(), configuration.c_str()  );
  return false;
}

bool setJointTeleop( ros::ServiceClient& client )
{
  return  !setConfiguration( client, "watch"            ) ? false
        : !setConfiguration( client, "full_watch"       ) ? false
        : !setConfiguration( client, "only_ctrl"    , 2 ) ? false
        :  setConfiguration( client, "joint_teleop"     ) ;
}

bool setJointInteporlated( ros::ServiceClient& client )
{
  return  !setConfiguration( client, "watch"                  ) ? false
        : !setConfiguration( client, "full_watch"             ) ? false
        : !setConfiguration( client, "only_ctrl"          , 2 ) ? false
        :  setConfiguration( client, "jpos_interpolated"      ) ;
}

bool setCartImpedanceTrajectory( ros::ServiceClient& client )
{
  return  !setConfiguration( client, "watch"                          ) ? false
        : !setConfiguration( client, "full_watch"                     ) ? false
        : !setConfiguration( client, "only_ctrl"                  , 2 ) ? false
        :  setConfiguration( client, "jpos_interpolated"          , 1 ) ? false
        :  setConfiguration( client, "cart_impedance_interpolated"    ) ;
}

bool setJointImpedanceTrajectory( ros::ServiceClient& client )
{
  return  !setConfiguration( client, "watch"                            ) ? false
        : !setConfiguration( client, "full_watch"                       ) ? false
        : !setConfiguration( client, "only_ctrl"                    , 2 ) ? false
        :  setConfiguration( client, "jpos_interpolated"            , 1 ) ? false
        :  setConfiguration( client, "joint_impedance_interpolated"     ) ;
}

bool setJointImpedance( ros::ServiceClient& client )
{
  return  !setConfiguration( client, "watch"                  ) ? false
        : !setConfiguration( client, "full_watch"             ) ? false
        : !setConfiguration( client, "only_ctrl"          , 2 ) ? false
        :  setConfiguration( client, "jpos_interpolated"  , 1 ) ? false
        :  setConfiguration( client, "joint_impedance"        ) ;
}
 
bool listConfiguration( ros::ServiceClient& client, std::vector<std::string>& configurations )
{
  configurations.clear();
  
  configuration_msgs::ListConfigurations srv;
  
  if (client.call(srv))
  {
    std::vector< configuration_msgs::ConfigurationComponent > confs = srv.response.configurations;
    for( const auto & c : confs )
    {
      configurations.push_back( c.name );
    }

    return true;
  }
  
  ROS_ERROR("Failed to call service '%s' ", client.getService().c_str() );
  return false;
}


bool resetFTSensor( ros::ServiceClient& client )
{
  std_srvs::Trigger srv;
  
  if (client.call(srv))
  {
    if( srv.response.success ) 
    {
      ROS_INFO("Request accepted.");
      return true;
    }
  }
  
  ROS_ERROR("Failed to call service '%s'", client.getService().c_str() );
  return false;
}


bool selectTrajectory( const std::vector<std::string>& list_trjs, std::string& trj_name )
{
  ROS_INFO ( "select trajectory:" );
  for ( unsigned int idx = 0; idx < list_trjs.size(); idx++ )
  {
      ROS_INFO ( "%d) %s", idx, list_trjs.at ( idx ).c_str() );
  }

  std::cout << "enter number: ";  std::string input;  std::cin >> input;

  int x;
  try
  {
      x = boost::lexical_cast<int> ( input );
  }
  catch ( boost::bad_lexical_cast const & )
  {
      std::cout << "Error: input string was not valid" << std::endl;
      return false;
  }

  if ( ( x < 0 ) || ( x >= list_trjs.size() ) )
  {
      ROS_ERROR ( "out of bound" );
      return false;
  }
  else
  {
      ROS_INFO ( "selected %d) %s", x, list_trjs.at ( x ).c_str() );
      trj_name = list_trjs.at ( x ).c_str();
  }
  return true;
}


bool selectConfiguration( const std::vector<std::string>& configurations, std::string& configuration )
{
  ROS_INFO ( "select configuration:" );
  ROS_INFO ( "0) no changes " );
  for ( unsigned int idx = 0; idx < configurations.size(); idx++ )
  {
      ROS_INFO ( "%d) %s", idx+1, configurations.at ( idx ).c_str() );
  }

  std::cout << "enter number: ";  std::string input;  std::cin >> input;

  int x;
  try
  {
    x = boost::lexical_cast<int> ( input );
  }
  catch ( boost::bad_lexical_cast const & )
  {
    std::cout << "Error: input string was not valid" << std::endl;
    return false;
  }

  if ( ( x < 0 ) || (x> configurations.size() ) )
  {
    ROS_ERROR_STREAM ( x << "is out of bound" );
    return false;
  }
  else
  {
    if(x == 0 )
    {
      ROS_INFO ( "selected 0) no changes " );
      configuration ="no-chage";
    }
    else
    {
      ROS_INFO ( "selected %d) %s", x, configurations.at ( x-1 ).c_str() );
      configuration = configurations.at ( x-1 ).c_str();
    }
  }
  
  return true;
}

const std::string ROBOT_DESCRIPTION = "/robot_description";

int main ( int argc, char **argv )
{

    ros::init ( argc, argv, "popeye_assembly" );
    ros::NodeHandle nh("~");
    ros::AsyncSpinner spinner ( 1 );
    spinner.start();
    
    ros::ServiceClient get_configurations  = nh.serviceClient<configuration_msgs::ListConfigurations> ("/configuration_manager/start_configuration");
    ros::ServiceClient start_configuration = nh.serviceClient<configuration_msgs::StartConfiguration> ("/configuration_manager/start_configuration");
    ros::ServiceClient reset_ftsensor      = nh.serviceClient<std_srvs::Trigger>                      ("/ati/ft/reset_offset");
    ros::Publisher     ovr_pub             = nh.advertise< std_msgs::Int64 >                          ("/speed_ovr", 1000);

    actionlib::SimpleActionClient<moveit_msgs::ExecuteTrajectoryAction> execute_trajectory_ac         ( nh, "/execute_trajectory" );
    if ( !execute_trajectory_ac.waitForServer ( ros::Duration ( 10 ) ) )
    {
        ROS_ERROR ( "NO /execute_trajectory server" );
        return 0;
    }
    std::string group_name;
    if ( !nh.getParam ( "group_name", group_name ) )
    {
      group_name = "manipulator";
    }
    
    
    moveit::planning_interface::MoveGroupInterface::Options opts(group_name, ROBOT_DESCRIPTION );
    moveit::planning_interface::MoveGroupInterface move_group ( opts );
    ROS_INFO ( "Reference frame   : %s", move_group.getPlanningFrame().c_str() );
    ROS_INFO ( "End effector link : %s", move_group.getEndEffectorLink().c_str() );

    std::vector<std::string> js = move_group.getActiveJoints();
    ROS_INFO ( "Controlled joint" );
    for ( std::string & s : js )
    {
        ROS_INFO ( "- %s", s.c_str() );
    }

    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    bool success;
   
    std::vector<std::string> executed_trjs;

    ROS_INFO ( "------------------" );
    std::vector<std::string> list_trjs;
    if ( !nh.getParam ( "trajectory_names", list_trjs ) )
    {
        ROS_ERROR ( "NO trajectory_names specified" );
        return -1;
    }
    
    const std::vector< std::string > configurations = { "jpos_interpolated", "cart_impedance_interpolated", "joint_impedance_interpolated" };

    for ( ;; )
    {
      std::string trj_name, configuration;
      if( !selectTrajectory( list_trjs, trj_name ) )
        continue;
      
      if(!selectConfiguration( configurations, configuration ) )
        continue;
      
      if( configuration == "no-chage" )
      {
      }
      else if( configuration == "jpos_interpolated" )  
      {
        if(!setJointInteporlated( start_configuration ) )
          return -1;
      }
      else if( configuration == "cart_impedance_interpolated" )
      {
        if( !setCartImpedanceTrajectory( start_configuration ) )
          return -1;
      }
      else if( configuration == "joint_impedance_interpolated" )
      {
        if( !setJointImpedanceTrajectory( start_configuration ) )
          return -1;
      }
      
      if( !resetFTSensor( reset_ftsensor ) )
        return -1;
      
      std_msgs::Int64 speed_ovr;  speed_ovr.data = 20;
      ovr_pub.publish( speed_ovr ); 
      
      //-------------------------------//
      move_group.startStateMonitor();
      moveit::core::RobotState trj_state = *move_group.getCurrentState();

      trajectory_msgs::JointTrajectory  trj;
      moveit_msgs::RobotTrajectory      isp_trj;
      moveit_msgs::RobotTrajectory      approach_trj;

      ROS_INFO("Loading from param ..");
      if ( !trajectory_processing::getTrajectoryFromParam ( nh, trj_name, trj ) )
      {
          ROS_ERROR ( "%s not found", trj_name.c_str() );
          return 0;
      }
      // Next get the current set of joint values for the group.
      std::vector<double> initial_position = trj.points.begin()->positions;
      ROS_INFO_STREAM( "Trajectory Initial Position : " << moveit_planning_helper::to_string( initial_position ) );

      bool is_single_point = false;
      if ( trj.points.size() < 2 )
      {
          isp_trj.joint_trajectory = trj;
          is_single_point = true;
      }
      else
      {
          trj_state.setJointGroupPositions ( group_name, initial_position );
          robot_trajectory::RobotTrajectory trajectory ( move_group.getRobotModel(), group_name );
          trajectory.setRobotTrajectoryMsg ( trj_state, trj );
          isp_trj.joint_trajectory = trj;
      }

      if ( isp_trj.joint_trajectory.points.back().time_from_start > trj.points.back().time_from_start )
      {
          ROS_WARN ( "trajectory is scaled to respect joint limit" );
      }

      // ---- APPROACH to initial pose
      move_group.setStartStateToCurrentState();
      robot_state::RobotStatePtr actual_state = move_group.getCurrentState(5);
      std::vector< double > actual_jpos(4,0);
      actual_state->copyJointGroupPositions(group_name, actual_jpos );
      ROS_INFO_STREAM( "Actual Robot Position : " << moveit_planning_helper::to_string( actual_jpos ) );
      move_group.setJointValueTarget ( initial_position );

      robot_trajectory::RobotTrajectory trajectory ( move_group.getRobotModel(), group_name );
      success = ( move_group.plan ( my_plan ) == moveit::planning_interface::MoveItErrorCode::SUCCESS );
      if ( !success )
      {
          ROS_ERROR ( "Planning failed" );
          return 0;
      }
      moveit_planning_helper::rvizDisplayPath(nh, my_plan);
      for(;;)
      {
        ROS_INFO("Press C to continue");
        std::string input;  std::cin >> input;
        char x;
        try
        {
          x = boost::lexical_cast<int> ( input );
          if( x == 'C' || x =='c' )
            break;
        }
        catch ( boost::bad_lexical_cast const & )
        {
          std::cout << "Error: input string was not valid" << std::endl;
          return false;
        }
        
      }
      

      trajectory.setRobotTrajectoryMsg ( trj_state, my_plan.trajectory_.joint_trajectory );
      trajectory_processing::IterativeSplineParameterization isp;
      isp.computeTimeStamps ( trajectory );
      trajectory.getRobotTrajectoryMsg ( approach_trj );

      ROS_INFO ( "Move %s to initial position", group_name.c_str() );
      std_srvs::Empty srv;
      moveit_msgs::ExecuteTrajectoryGoal goal;
      if ( is_single_point )
      {
        ROS_INFO ( "Start Approach Movement");
        goal.trajectory = approach_trj;
        actionlib::SimpleClientGoalState state = execute_trajectory_ac.sendGoalAndWait ( goal );
        ROS_INFO_STREAM ( "Approach Movement " << state.toString() );
      }
      else
      {
        ROS_INFO ( "Start Approach Movement" );
        goal.trajectory = approach_trj;
        if( !resetFTSensor( reset_ftsensor ) )  return -1;
        actionlib::SimpleClientGoalState state = execute_trajectory_ac.sendGoalAndWait ( goal );
        ROS_INFO_STREAM ( "Approach Movement " << state.toString() );
        if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
        {
          ros::Duration ( 1 ).sleep();
          if( !resetFTSensor( reset_ftsensor ) )  return -1;
          ROS_INFO ( "Execute trajectory %s", trj_name.c_str() );
          goal.trajectory = isp_trj;
          actionlib::SimpleClientGoalState state2 = execute_trajectory_ac.sendGoalAndWait ( goal );
          ROS_INFO_STREAM ( "Trajectory " << trj_name << "state: " << state2.toString() );
          if( state != actionlib::SimpleClientGoalState::SUCCEEDED )
          {
            ROS_INFO_STREAM ( "Trajectory " << trj_name << " execution Failed. Try again." );
          }
        }
        else
        {
          ROS_WARN( "Approach Movement FAILED. Try again" );
        }
      }

      ros::Duration ( 1 ).sleep();

    }
    ros::shutdown();
    return 0;
}




