#include <popeye_assembly/task_manager.h>
#include <moveit/robot_state/conversions.h>
#include <popeye_assembly/common.h>
#include <eigen_conversions/eigen_msg.h>
#include <eigen_conversions/eigen_kdl.h>
#include <tf_conversions/tf_eigen.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_planning_helper/manage_planning_scene.h>
#include <tf/transform_datatypes.h>

#define GET_AND_GO_TO_ERROR( param_name, param )\
if (!nh_.getParam(param_name,param))\
{\
  ROS_ERROR("Parameter '[%s]/[%s]' is not in the ros parame server. Go To Error State.", nh_.getNamespace().c_str(), std::string( param_name ).c_str() );\
  go_to_error_=true;\
  return;\
}\



namespace popeye_assembly
{
namespace task
{

StateMachineStructure::StateMachineStructure( ros::NodeHandle& nh )  
    : mp_(nh)
{  

}

void StateMachineStructure::entryStateIdle( )
{
}

void StateMachineStructure::entryStatePlan( )
{

  if( !mp_.planPrepare( pick_place_goal_ ) )
  {
    is_planning_failed_ = true;
    go_to_error_ = true;
    return;
  }
  
  const std::vector<int32_t> steps= { popeye_assembly::PickPlanGoal::APPROACH_A
                                    , popeye_assembly::PickPlanGoal::APPROACH_B
                                    , popeye_assembly::PickPlanGoal::GRASP
                                    , popeye_assembly::PickPlanGoal::GRASP_FINALIZATION
                                    , popeye_assembly::PickPlanGoal::CARRY
                                    , popeye_assembly::PickPlanGoal::PLACE_A
                                    , popeye_assembly::PickPlanGoal::PLACE_B
                                    , popeye_assembly::PickPlanGoal::PLACE_FINALIZATION
                                    , popeye_assembly::PickPlanGoal::GO_TO_REST  };
  
  is_planning_failed_= !mp_.planComputation( steps, motion_goals_ );

  return;
}

void StateMachineStructure::entryStateReplan ( )
{
  is_planning_failed_= false;

  // is_planning_failed_= !mp_.planComputation( motion_goals_ );
}
void StateMachineStructure::entryStateWait  ( ){ }
void StateMachineStructure::entryStateError ( ){ }

void StateMachineStructure::exitStateInit   ( ){ }
void StateMachineStructure::exitStateIdle   ( ){ }
void StateMachineStructure::exitStatePlan   ( ){ }
void StateMachineStructure::exitStateReplan ( ){ }
void StateMachineStructure::exitStateWait   ( ){ }
void StateMachineStructure::exitStateError  ( ){ }

void StateMachineStructure::entryStateInit()
{
  is_planning_failed_ = false;
  go_to_error_        = !mp_.init();
  
}

void StateMachineStructure::actionStorePlanPlanRequest( pickPlanRequest const & e )
{
  pick_place_goal_ = e.pick_place_goal_;
}
void StateMachineStructure::actionSendPlan ( none const & e )
{
  if (!motion_fsm_)
  {
    ROS_ERROR("motion fsm is not initialized");
    is_planning_failed_ = true;
  }
  motion_fsm_->process_event( popeye_assembly::motion::pickRequest( motion_goals_ ) );
  ROS_INFO("Send plan");
}
bool StateMachineStructure::guardIsTimeExpired  ( none const & e )
{
  ROS_INFO("ehi sono passato di qui");
  if (((start_time_ - ros::Time::now()).toSec()<0))
    ROS_ERROR("ma troppo tardi (%f secondi fa) ", (ros::Time::now()-start_time_).toSec());
  else
    ROS_INFO("posso aspettare %f secondi", (start_time_-ros::Time::now()).toSec());
  return ((start_time_-ros::Time::now()).toSec()<0);
}
bool StateMachineStructure::guardIsInError      ( none const & e )
{
  return go_to_error_;
}
bool StateMachineStructure::guardIsPlanFailed   ( none const & e )
{
  return is_planning_failed_;
}
bool StateMachineStructure::guardIsPlanSuccess  ( none const & e )
{
  return !is_planning_failed_;
}
bool StateMachineStructure::guardIsObjectCorrect( pickExecuteRequest const & e )
{
  return true;
}

}
}
