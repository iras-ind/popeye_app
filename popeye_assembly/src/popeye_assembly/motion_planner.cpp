#include <popeye_assembly/motion_planner.h>
#include <moveit/robot_state/conversions.h>
#include <popeye_assembly/common.h>
#include <eigen_conversions/eigen_msg.h>
#include <eigen_conversions/eigen_kdl.h>
#include <tf_conversions/tf_eigen.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_planning_helper/manage_planning_scene.h>
#include <tf/transform_datatypes.h>

#define GET_AND_GO_TO_ERROR( param_name, param )\
if (!nh_.getParam(param_name,param))\
{\
  ROS_ERROR("Parameter '[%s]/[%s]' is not in the ros parame server. Go To Error State.", nh_.getNamespace().c_str(), std::string( param_name ).c_str() );\
  return false;\
}\



namespace popeye_assembly
{

MotionPlanner::MotionPlanner( ros::NodeHandle& nh )  
: nh_(nh) 
{  
  attach_object_client_ = nh_.serviceClient<popeye_assembly::attach_object>(ATTACH_OBJECT_SRV);
}


bool MotionPlanner::init( )
{

  
  // TODO INIT
  tool_frame_name_ = "ur10_tool0";
  base_frame_name_ = "/world";

  bool use_ikfast=true;
  bool consider_starting_point=true;
  
  GET_AND_GO_TO_ERROR( "group_name"          , group_name_             )
  GET_AND_GO_TO_ERROR( "robot_description"   , robot_description_      )
  
  GET_AND_GO_TO_ERROR( "use_ikfast" , use_ikfast              )
  if( use_ikfast )
  {
    GET_AND_GO_TO_ERROR( group_name_ + "/ikfast_base_frame" , base_frame_name_        )
    GET_AND_GO_TO_ERROR( group_name_ + "/ikfast_tool_frame" , tool_frame_name_        )
  }
  
  GET_AND_GO_TO_ERROR( "consider_starting_point"          , consider_starting_point )
  
  robot_model_loader_.reset( new robot_model_loader::RobotModelLoader ( robot_description_  ) );
  robot_model_        = robot_model_loader_->getModel();
  planning_scene_    .reset( new planning_scene::PlanningScene( robot_model_ ) );
  base_frame_name_    = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getBaseFrame();
  tool_frame_name_    = robot_model_->getJointModelGroup(group_name_)->getSolverInstance()->getTipFrame();

  moveit::planning_interface::MoveGroupInterface::Options opts(group_name_, robot_description_);
  move_group_.reset(new moveit::planning_interface::MoveGroupInterface(opts));
  while( !move_group_->startStateMonitor() )
  {
    ROS_WARN_THROTTLE(5,"Waiting for the start of the state monitor");
    ros::Duration(0.5).sleep();
  }

  attached_ = false;
  robot_jnames_ = robot_model_->getJointModelGroup(group_name_)->getVariableNames();
  robot_state_.reset(new robot_state::RobotState(robot_model_) );
  robot_state_  = move_group_->getCurrentState();
  robot_state_->copyJointGroupPositions(robot_model_->getJointModelGroup(group_name_), robot_start_jconf_);
  for (std::size_t i = 0; i < robot_jnames_.size(); ++i)
  {
    ROS_INFO("Start jconf: %s: %f", robot_jnames_[i].c_str(), robot_start_jconf_[i]);
  }
  
  cart_trj_helper_.reset( new descartes::CartesianTrajectoryHelper( robot_description_
                                                                  , group_name_
                                                                  , base_frame_name_
                                                                  , tool_frame_name_
                                                                  , false
                                                                  , true ) );


  
  return true;
}

bool MotionPlanner::planPrepare( const popeye_assembly::PickPlanGoal& pick_place_goal )
{
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "--    Check input poses      --" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );
  const std::vector<int32_t> steps= { popeye_assembly::PickPlanGoal::APPROACH_A
                                    , popeye_assembly::PickPlanGoal::APPROACH_B
                                    , popeye_assembly::PickPlanGoal::GRASP
                                    , popeye_assembly::PickPlanGoal::GRASP_FINALIZATION
                                    , popeye_assembly::PickPlanGoal::CARRY
                                    , popeye_assembly::PickPlanGoal::PLACE_A
                                    , popeye_assembly::PickPlanGoal::PLACE_B
                                    , popeye_assembly::PickPlanGoal::PLACE_FINALIZATION
                                    , popeye_assembly::PickPlanGoal::GO_TO_REST };

  kinematics::KinematicsBasePtr solver = robot_model_->getJointModelGroup ( group_name_ )->getSolverInstance();
  for(  size_t i=0; i<steps.size(); i++ )
  {
    const int32_t& step = steps[i];
    bool          carried_object_attached = true;   
    
    ROS_INFO_STREAM(">> Step: " << BOLDYELLOW() << popeye_assembly::AssemblyStep::to_string( step ) << RESET() );
    
    tf::transformMsgToTF(pick_place_goal.steps[step].target_frame, target_conf_[step]);

    execution_time_          [step] = pick_place_goal.steps[step].time;
    discretization_pts_      [step] = pick_place_goal.steps[step].discretization_pts;
    controller_configuration_[step] = pick_place_goal.steps[step].controller_configuration;

    if( step == popeye_assembly::PickPlanGoal::APPROACH_A )
    {
      start_jconf_ [step] = robot_start_jconf_;
      robot_state_->setJointGroupPositions( group_name_, start_jconf_ [step] );
      
      const auto e = robot_state_->getFrameTransform(tool_frame_name_);
      tf::transformEigenToTF(e, start_conf_[step]);
      
    }
    else
    {
      start_conf_   [step] = target_conf_   [ popeye_assembly::AssemblyStep::prev( step ) ];
      start_jconf_  [step] = target_jconf_  [ popeye_assembly::AssemblyStep::prev( step ) ];

    }

    geometry_msgs::Pose pose;
    tf::poseTFToMsg(target_conf_[step], pose);
    ROS_INFO_STREAM("Target Configuration:\n" << moveit_planning_helper::to_string( pose ) );
    
    if( !moveit_planning_helper::manageCollisions( root_nh_
                                                  , robot_model_
                                                  , { "hatbox"
                                                    , "hatbox_approach_A"            
                                                    , "hatbox_approach_B"            
                                                    , "hatbox_bracket"             
                                                    , "hatbox_escape"              
                                                    , "fuselage_hatbox_bracket_1"  
                                                    , "fuselage_hatbox_approach_1_A" 
                                                    , "fuselage_hatbox_approach_1_B" 
                                                    , "fuselage_hatbox_final_1"
                                                    , "fuselage" }
                                                ,   { "hatbox_clamper"
                                                    , "hatbox" }
                                                , true ) )
    {
      ROS_ERROR("UFFA" );
    }
    
    if ( !robot_state_->setFromIK (robot_model_->getJointModelGroup(group_name_), pose, 10, 10.0) )
    {
      ROS_FATAL_STREAM("Error IK. Out of ws?" );
      ROS_FATAL_STREAM("<< Step: " << popeye_assembly::AssemblyStep::to_string( step ) );
      ROS_INFO_STREAM( BOLDYELLOW() << "-----------------------------------" << RESET() );
      ROS_INFO_STREAM( BOLDYELLOW() << "--  Check input poses " << BOLDRED() << " FAILED" << BOLDYELLOW()  << "  --" << RESET() );
      ROS_INFO_STREAM( BOLDYELLOW() << "-----------------------------------" << RESET() );

      return false;
    }
    
    target_jconf_ [step].resize( solver->getJointNames().size(), 0.0 );
    robot_state_->copyJointGroupPositions(group_name_, target_jconf_[step]);
    
    ROS_INFO_STREAM("Start  Joint Configuration:" << BOLDYELLOW() << moveit_planning_helper::to_string( start_jconf_ [step] ) );
    ROS_INFO_STREAM("Target Joint Configuration:" << BOLDYELLOW() << moveit_planning_helper::to_string( target_jconf_[step] ) );
    ROS_INFO_STREAM("Start        Configuration:" << BOLDYELLOW() << moveit_planning_helper::to_string( start_conf_  [step] ) );
    ROS_INFO_STREAM("Target       Configuration:" << BOLDYELLOW() << moveit_planning_helper::to_string( target_conf_ [step] ) );
    ROS_INFO_STREAM("<< Step: " << BOLDYELLOW() << popeye_assembly::AssemblyStep::to_string( step ) << RESET() );
  }
  
  if( !moveit_planning_helper::manageCollisions( root_nh_, robot_model_, { "hatbox_clamper", "fuselage" }, { "hatbox" } , false, true ) )
  {
    ROS_ERROR("UFFA" );
  }
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "--  Check input poses " << BOLDGREEN() << " OK" << BOLDYELLOW()  << "  --" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );

  
  return true;
}

/**
 * 
 * 
 * 
 * 
 * 
 */
bool MotionPlanner::planComputation( const std::vector<int32_t>& steps, std::map<int32_t, std::shared_ptr<AssemblyStep> >& motion_goals)
{
  
  motion_goals.clear(); 
  ros::Publisher display_publisher = nh_.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
  moveit_msgs::DisplayTrajectory display_trajectory;
  
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "--            Plan           --" << RESET() );
  ROS_INFO_STREAM( BOLDYELLOW() << "-------------------------------" << RESET() );

  for( size_t i=0; i<steps.size(); i++)
  {
    const int32_t& step = steps[i];
    ROS_INFO_STREAM(">> Step: " << BOLDYELLOW() << popeye_assembly::AssemblyStep::to_string( step ) << RESET() );

    if( step == popeye_assembly::PickPlanGoal::APPROACH_A )
    {
      target_jconf_[step] = start_jconf_ [popeye_assembly::AssemblyStep::next( step )];
    }
    else if( step == popeye_assembly::PickPlanGoal::GO_TO_REST )
    {
      start_jconf_ [step] = target_jconf_[popeye_assembly::AssemblyStep::prev( step )];
    }
    else
    {
      start_jconf_ [step] = target_jconf_[popeye_assembly::AssemblyStep::prev( step )];
      target_jconf_[step] = start_jconf_ [popeye_assembly::AssemblyStep::next( step )];
    }

    ROS_INFO_STREAM("Start  Joint Configuration: "<< BOLDYELLOW() << moveit_planning_helper::to_string( start_jconf_ [step] ) << RESET() );
    ROS_INFO_STREAM("Target Joint Configuration: "<< BOLDYELLOW() << moveit_planning_helper::to_string( target_jconf_[step] ) << RESET() );
    moveit::planning_interface::MoveGroupInterface::Plan  plan;
    
    robot_state_->setJointGroupPositions( group_name_, start_jconf_[step] );
    moveit::core::robotStateToRobotStateMsg( *robot_state_, plan.start_state_ );
    
    if( !moveit_planning_helper::manageCollisions ( root_nh_
                                                  , robot_model_
                                                  , { "hatbox_clamper", "hatbox_cart" }
                                                  , { "hatbox" }
                                                  , (step != popeye_assembly::PickPlanGoal::APPROACH_B)
                                                  , true) )
    {
      ROS_ERROR("UFFA" );
    }
    
    if( step == popeye_assembly::PickPlanGoal::GRASP_FINALIZATION )
    {
      if(!attachHatbox( "hatbox_clamper" ) )
        return false;
    }
    else if( step == popeye_assembly::PickPlanGoal::GO_TO_REST )
    {
      if(!attachHatbox( "fuselage_hatbox_final_1"  ) )
        return false;
    }

    switch(step)
    {
      case popeye_assembly::PickPlanGoal::GRASP_FINALIZATION:
      case popeye_assembly::PickPlanGoal::PLACE_A:
      case popeye_assembly::PickPlanGoal::PLACE_B:
      case popeye_assembly::PickPlanGoal::PLACE_FINALIZATION:
      {
        std::vector<geometry_msgs::Pose> waypoints(2);

        tf::poseTFToMsg(start_conf_ [ step ], waypoints[0] );
        tf::poseTFToMsg(target_conf_[ step ], waypoints[1] );
        
        double fraction = move_group_->computeCartesianPath(waypoints, 0.005,  0.0,  plan.trajectory_);

        if( fraction < 0.95 )
        {
          ROS_INFO("Cartesian path: %.2f%% acheived",  fraction * 100.0);

          auto last_point = plan.trajectory_.joint_trajectory.points.back().positions;
          std::vector<double> dist( last_point.size(), 0 );
          std::transform( target_jconf_[step].begin(), target_jconf_[step].end(), last_point.begin(), dist.begin(), []( double jn, double jf ) { return std::fabs( jn - jf); } );
          ROS_INFO_STREAM("Distance from target: " << moveit_planning_helper::to_string(dist) );
          
          Eigen::Affine3d T_base_A; tf::poseMsgToEigen(waypoints[0], T_base_A );
          Eigen::Affine3d T_base_B; tf::poseMsgToEigen(waypoints[0], T_base_B );
          
          if (!cart_trj_helper_->computeCartesianTrajectory( T_base_A
                                                            , T_base_B
                                                            , execution_time_     [ step ]
                                                            , discretization_pts_ [ step ]
                                                            , plan.trajectory_.joint_trajectory
                                                            , &start_jconf_[ step ] ) )
          {
            ROS_WARN("None trajectory has been found. New trial with starting condiion relaxed");


            if (!cart_trj_helper_->computeCartesianTrajectory ( T_base_A
                                                              , T_base_B
                                                              , execution_time_ [ step ]
                                                              , discretization_pts_ [ step ]
                                                              , plan.trajectory_.joint_trajectory ) )
            {
              ROS_ERROR("Descartes planning failed also with starting configuration relaxed. NO Cart trajectory can be found.");
              return false;
            }
            else
            {
                start_jconf_ [step] = plan.trajectory_.joint_trajectory.points.front().positions;
                target_jconf_[step] = plan.trajectory_.joint_trajectory.points.back().positions;
            }
          }
          
        }
        
        
        
        
        start_jconf_ [step] = plan.trajectory_.joint_trajectory.points.front().positions;
        target_jconf_[step] = plan.trajectory_.joint_trajectory.points.back().positions;
        break;

      }
      break;
      case popeye_assembly::PickPlanGoal::APPROACH_A:
      case popeye_assembly::PickPlanGoal::APPROACH_B:
      case popeye_assembly::PickPlanGoal::GRASP:
      case popeye_assembly::PickPlanGoal::CARRY:
      case popeye_assembly::PickPlanGoal::GO_TO_REST:
      {
          move_group_ ->setJointValueTarget   ( target_jconf_[step] );
          move_group_ ->setPlanningTime( 20.0 );
          
          
          bool success = (move_group_->plan( plan ) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
          if (!success)
          {
            ROS_ERROR("Plan failed...");
            return false;
          }
          
          // RESAMPLING
          trajectory_processing::IterativeSplineParameterization isp;
          for (trajectory_msgs::JointTrajectoryPoint& pnt: plan.trajectory_.joint_trajectory.points)
            pnt.time_from_start=ros::Duration(0);

          robot_trajectory::RobotTrajectory trj(move_group_->getRobotModel(),group_name_);
          trj.setRobotTrajectoryMsg( *robot_state_, plan.trajectory_);
          isp.computeTimeStamps(trj);
          trj.getRobotTrajectoryMsg(plan.trajectory_);


        }
        break;
      }
      
      ROS_INFO(" ***************** EXECUTE TRAJECTORY ******************* [hit any key]" );
      moveit::planning_interface::MoveGroupInterface::Plan  sim_plan;
      std::cin.get();
            
      /* Visualize the trajectory */
      ROS_INFO("Moving the robot (is in simulation, isn't??");
      moveit::planning_interface::MoveItErrorCode err = move_group_->execute( plan );
      if( err != moveit_msgs::MoveItErrorCodes::SUCCESS )
      {
        ROS_ERROR("Failed to move the robot ...." );
        std::cout << plan.trajectory_ << std::endl;
        return false;
      }
      
      control_msgs::FollowJointTrajectoryGoal tg;
      tg.trajectory = plan.trajectory_.joint_trajectory;
      motion_goals[ step ].reset(
            new AssemblyStep  ( tg, ros::Duration( execution_time_ [ step ]), controller_configuration_[step] )
            );

    }
  
  ROS_INFO_STREAM( BOLDYELLOW() << "Plan" << BOLDGREEN() << " OK" << RESET() );
  return true;
}


  
bool MotionPlanner::attachHatbox( const std::string& link_to_attach )
{
  popeye_assembly::attach_object srv;
  srv.request.object_id = "hatbox";
  srv.request.link_to_attach = link_to_attach ;
  
  if (attach_object_client_.call(srv))
  {
    if( !srv.response.success )
    {
      ROS_ERROR("Failed to call service %s", attach_object_client_.getService().c_str() );
      return false;
    }
  }
  else
  {
    ROS_ERROR("Failed to call service %s", attach_object_client_.getService().c_str() );
    return false;
  }

  return true;
}

}
